<?php

namespace App\Http\Controllers\Web\User;

use App\Beranda;
use App\Gambar;
use App\Http\Controllers\Controller;
use App\Profil;
use Illuminate\Http\Request;

class LaporanController extends Controller
{
    public function index()
    {
        $profil = Profil::all();
        $gambar = Gambar::all();
        $beranda = Beranda::all();

        $page = 'laporan';
        
        return view('user.laporan',compact('page','profil','beranda','gambar'));
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
