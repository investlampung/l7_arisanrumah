<?php

namespace App\Http\Controllers\Web\User;

use App\Album;
use App\Beranda;
use App\Gambar;
use App\History;
use App\Http\Controllers\Controller;
use App\Http\Requests\HubungiRequest;
use App\Pertanyaan;
use App\Pesan;
use App\Photo;
use App\Prinsip;
use App\Profil;
use App\Progres;
use App\Testimoni;
use App\Video;
use Illuminate\Http\Request;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class BerandaController extends Controller
{

    public function index()
    {
        $beranda = Beranda::all();
        $gambar = Gambar::all();
        $profil = Profil::all();
        $video = Video::where('id', 'not like', 1)->orderBy('id', 'DESC')->paginate(3);
        $prinsip = Prinsip::all();
        $testimoni = Testimoni::orderBy('id', 'DESC')->paginate(4);
        $progres = Progres::orderBy('id', 'DESC')->paginate(6);
        $tanya = Pertanyaan::all();
        $photo = Photo::orderBy('id', 'DESC')->paginate(8);
        $viddd = Video::where('id', 1)->get()->first();

        $no = 1;

        $page = 'beranda';

        return view('user.beranda', compact('no', 'page', 'beranda', 'testimoni', 'gambar', 'profil', 'video', 'prinsip', 'photo', 'progres', 'tanya', 'viddd'));
    }

    public function create()
    {
        //
    }

    public function download()
    {
        $file = 'itlabil/file/katalog.pdf';
        $name = basename($file);
        return response()->download($file, $name);
    }

    public function hubungi()
    {
        $gambar = Gambar::all();
        return view('user.hubungi', compact('gambar'));
    }

    public function store(HubungiRequest $request)
    {
        $data = $request->only('nama', 'pesan', 'telp', 'email', 'nik', 'marketing');

        if ($request->hasFile('photo')) {
            $data['photo'] = $this->savePhoto($request->file('photo'));
        }

        Pesan::create($data);

        // HISTORI
        $histori['user'] = $request->nama;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "User " . $request->nama . " mengirim data diri " . $request->pesan;
        History::create($histori);

        $notification = array(
            'message' => 'Data berhasil dikirim',
            'alert-type' => 'success'
        );

        return redirect('/')->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    public function gallery()
    {

        $beranda = Beranda::all();
        $gambar = Gambar::all();
        $profil = Profil::all();
        $photo = Photo::paginate(20);
        $page = 'gallery';
        return view('user.gallery', compact('page', 'photo', 'gambar', 'beranda', 'profil'));
    }

    public function video()
    {

        $beranda = Beranda::all();
        $gambar = Gambar::all();
        $profil = Profil::all();
        $video = Video::where('id', 'not like', 1)->paginate(9);
        $page = 'video';
        return view('user.video', compact('page', 'video', 'gambar', 'beranda', 'profil'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    protected function savePhoto(UploadedFile $photo)
    {
        $fileName = Str::random(10) . '.' . $photo->guessClientExtension();
        $destinationPath = 'itlabil/images/pesan';
        $photo->move($destinationPath, $fileName);
        return $fileName;
    }

    public function deletePhoto($filename)
    {
        $path = 'itlabil/images/pesan/' . $filename;
        return File::delete($path);
    }
}
