<?php

namespace App\Http\Controllers\Web\Admin;

use App\Gambar;
use App\History;
use App\Http\Controllers\Controller;
use App\Pesan;
use App\Profil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;
use Illuminate\Support\Str;

class PesanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $gambar = Gambar::all();
        $no = 1;
        $data = Pesan::orderBy('id', 'DESC')->get()->all();

        return view('admin.pesan.beranda', compact('no', 'data', 'gambar'));
    }

    public function create()
    {
        //
    }

    public function pesan(Request $request, $id)
    {
        $profil = Profil::all();
        $data = Pesan::where('id', $id)->get()->first();

        return view('admin.pesan.print_pesan', compact('data', 'profil'));
    }

    public function pesansemua()
    {
        $profil = Profil::all();
        $data = Pesan::orderBy('id', 'DESC')->get()->all();

        return view('admin.pesan.print_pesan_semua', compact('data', 'profil'));
    }

    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pesan $pesan)
    {
        $this->deletePhoto($pesan->photo);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Hapus";
        $histori['desc_info'] = "Data Pesan " . $pesan->nama;
        History::create($histori);

        $notification = array(
            'message' => 'Data Pesan "' . $pesan->nama . '" berhasil dihapus.',
            'alert-type' => 'error'
        );

        $pesan->delete();

        return redirect()->route('pesan.index')->with($notification);
    }

    public function deletePhoto($filename)
    {
        $path = 'itlabil/images/pesan/' . $filename;
        return File::delete($path);
    }
}
