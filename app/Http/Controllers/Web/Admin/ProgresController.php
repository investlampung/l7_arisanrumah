<?php

namespace App\Http\Controllers\Web\Admin;

use App\Gambar;
use App\History;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProgresRequest;
use App\Progres;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;
use Illuminate\Support\Str;

class ProgresController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $gambar = Gambar::all();
        $data = Progres::all();
        return view('admin.progres.beranda', compact('data', 'gambar'));
    }

    public function create()
    {
        //
    }

    public function store(ProgresRequest $request)
    {
        $data = $request->only('judul');

        if ($request->hasFile('photo')) {
            $data['photo'] = $this->savePhoto($request->file('photo'));
        }

        Progres::create($data);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Data Progres " . $request->judul;
        History::create($histori);

        $notification = array(
            'message' => 'Data Progres "' . $request->judul . '" berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->route('progres.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $progres = Progres::findOrFail($id);
        $gambar = Gambar::all();
        return view('admin.progres.edit', compact('progres', 'gambar'));
    }

    public function update(Request $request, $id)
    {
        $progres = Progres::findOrFail($id);

        $data = $request->only('judul');

        if ($request->hasFile('photo')) {
            $this->deletePhoto($progres->photo);
            $data['photo'] = $this->savePhoto($request->file('photo'));
        }

        $progres->update($data);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Data Progres " . $progres->judul;
        History::create($histori);

        $notification = array(
            'message' => 'Data Progres "' . $progres->judul . '" berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('progres.index')->with($notification);
    }

    public function destroy($id)
    {

        $progres = Progres::findOrFail($id);
        $this->deletePhoto($progres->photo);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Hapus";
        $histori['desc_info'] = "Data Progress " . $progres->judul;
        History::create($histori);

        $notification = array(
            'message' => 'Data Progres ' . $progres->judul . ' berhasil dihapus.',
            'alert-type' => 'error'
        );

        $progres->delete();

        return redirect()->route('progres.index')->with($notification);
    }

    protected function savePhoto(UploadedFile $photo)
    {
        $fileName = Str::random(10) . '.' . $photo->guessClientExtension();
        $destinationPath = 'itlabil/images/progres';
        $photo->move($destinationPath, $fileName);
        return $fileName;
    }

    public function deletePhoto($filename)
    {
        $path = 'itlabil/images/progres/' . $filename;
        return File::delete($path);
    }
}
