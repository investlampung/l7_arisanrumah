<?php

namespace App\Http\Controllers\Web\Admin;

use App\Gambar;
use App\History;
use App\Http\Controllers\Controller;
use App\Http\Requests\TimRequest;
use App\Tim;
use Illuminate\Http\Request;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class TimController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $gambar = Gambar::all();
        $data = Tim::all();
        return view('admin.tim.beranda', compact('data','gambar'));
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TimRequest $request)
    {
        $data = $request->only('wilayah', 'nama', 'jabatan', 'telp', 'email');

        if ($request->hasFile('photo')) {
            $data['photo'] = $this->savePhoto($request->file('photo'));
        } else {
            $data['photo'] = 'avatar.png';
        }

        Tim::create($data);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Data Tim Marketing " . $request->nama;
        History::create($histori);

        $notification = array(
            'message' => 'Data Tim Marketing "' . $request->nama . '" berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->route('tim.index')->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function edit(Tim $tim)
    {
        $gambar = Gambar::all();
        return view('admin.tim.edit', compact('tim','gambar'));
    }

    public function update(Request $request, $id)
    {
        $tim = Tim::findOrFail($id);

        $data = $request->only('wilayah', 'nama', 'jabatan', 'telp', 'email');

        if ($request->hasFile('photo')) {
            if ($tim->photo == 'avatar.png') {
            } else {
                $this->deletePhoto($tim->photo);
            }
            $data['photo'] = $this->savePhoto($request->file('photo'));
        }

        $tim->update($data);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Data Tim Marketing " . $tim->nama;
        History::create($histori);

        $notification = array(
            'message' => 'Data Tim Marketing "' . $tim->nama . '" berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('tim.index')->with($notification);
    }

    public function destroy(Tim $tim)
    {
        if ($tim->photo == 'avatar.png') {
        } else {
            $this->deletePhoto($tim->photo);
        }

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Hapus";
        $histori['desc_info'] = "Data Tim Marketing " . $tim->nama;
        History::create($histori);

        $notification = array(
            'message' => 'Data Tim Marketing "' . $tim->nama . '" berhasil dihapus.',
            'alert-type' => 'error'
        );

        $tim->delete();

        return redirect()->route('tim.index')->with($notification);
    }

    protected function savePhoto(UploadedFile $photo)
    {
        $fileName = Str::random(10) . '.' . $photo->guessClientExtension();
        $destinationPath = 'itlabil/images/tm';
        $photo->move($destinationPath, $fileName);
        return $fileName;
    }

    public function deletePhoto($filename)
    {
        $path = 'itlabil/images/tm/' . $filename;
        return File::delete($path);
    }
}
