<?php

namespace App\Http\Controllers\Web\Admin;

use App\Gambar;
use App\History;
use App\Http\Controllers\Controller;
use App\Http\Requests\TestimoniRequest;
use App\Testimoni;
use Illuminate\Http\Request;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class TestimoniController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $no=1;
        $gambar = Gambar::all();
        $data = Testimoni::orderBy('id', 'DESC')->get()->all();
        return view('admin.testimoni.beranda', compact('data', 'gambar','no'));
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('photo')) {
            $data['photo'] = $this->savePhoto($request->file('photo'));
        }

        Testimoni::create($data);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Data Testimoni.";
        History::create($histori);

        $notification = array(
            'message' => 'Data Testimoni berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->route('testimoni.index')->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $testimoni = Testimoni::findOrFail($id);
        $gambar = Gambar::all();
        return view('admin.testimoni.edit', compact('testimoni', 'gambar'));
    }

    public function update(Request $request, $id)
    {
        $progres = Testimoni::findOrFail($id);

        if ($request->hasFile('photo')) {
            $this->deletePhoto($progres->photo);
            $data['photo'] = $this->savePhoto($request->file('photo'));
        }

        $progres->update($data);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Data Testimoni.";
        History::create($histori);

        $notification = array(
            'message' => 'Data Testimoni berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('testimoni.index')->with($notification);
    }

    public function destroy($id)
    {

        $progres = Testimoni::findOrFail($id);
        $this->deletePhoto($progres->photo);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Hapus";
        $histori['desc_info'] = "Data Testimoni.";
        History::create($histori);

        $notification = array(
            'message' => 'Data Testimoni berhasil dihapus.',
            'alert-type' => 'error'
        );

        $progres->delete();

        return redirect()->route('testimoni.index')->with($notification);
    }

    protected function savePhoto(UploadedFile $photo)
    {
        $fileName = Str::random(10) . '.' . $photo->guessClientExtension();
        $destinationPath = 'itlabil/images/testimoni';
        $photo->move($destinationPath, $fileName);
        return $fileName;
    }

    public function deletePhoto($filename)
    {
        $path = 'itlabil/images/testimoni/' . $filename;
        return File::delete($path);
    }
}
