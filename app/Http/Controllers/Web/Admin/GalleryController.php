<?php

namespace App\Http\Controllers\Web\Admin;

use App\Gambar;
use App\History;
use App\Http\Controllers\Controller;
use App\Photo;
use Illuminate\Http\Request;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class GalleryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $gambar = Gambar::all();
        $data = Photo::orderBy('id', 'DESC')->paginate(12);
        return view('admin.photo.beranda', compact('data','gambar'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {

        if ($request->hasFile('photo')) {
            $data['photo'] = $this->savePhoto($request->file('photo'));
        }

        Photo::create($data);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Photo Gallery ";
        History::create($histori);

        $notification = array(
            'message' => 'Photo Gallery berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->route('photo.index')->with($notification);
    }

    public function show($id)
    {
        //
    }

    public function edit(Gambar $gambar)
    {
    }

    public function update(Request $request, $id)
    {
    }

    public function destroy(Photo $photo)
    {
        $this->deletePhoto($photo->photo);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Hapus";
        $histori['desc_info'] = "Photo Gallery";
        History::create($histori);

        $notification = array(
            'message' => 'Photo Gallery berhasil dihapus.',
            'alert-type' => 'error'
        );

        $photo->delete();

        return redirect()->route('photo.index')->with($notification);
    }

    protected function savePhoto(UploadedFile $photo)
    {
        $fileName = Str::random(10) . '.' . $photo->guessClientExtension();
        $destinationPath = 'itlabil/images/photo';
        $photo->move($destinationPath, $fileName);
        return $fileName;
    }

    public function deletePhoto($filename)
    {
        $path = 'itlabil/images/photo/' . $filename;
        return File::delete($path);
    }
}
