<?php

namespace App\Http\Controllers\Web\Admin;

use App\Gambar;
use App\History;
use App\Http\Controllers\Controller;
use App\Profil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfilController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $gambar = Gambar::all();
        $data = Profil::all();
        return view('admin.profil.beranda', compact('data','gambar'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Profil $profil)
    {
        $gambar = Gambar::all();
        return view('admin.profil.edit', compact('profil','gambar'));
    }

    public function update(Request $request, Profil $profil)
    {
        $profil->update($request->all());

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Profil " . $profil->profil_key . " menjadi " . $profil->profil_value;
        History::create($histori);

        $notification = array(
            'message' => 'Profil "' . $profil->profil_key . '" berhasil diubah menjadi ' . $profil->profil_value,
            'alert-type' => 'success'
        );

        return redirect()->route('profil.index')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
