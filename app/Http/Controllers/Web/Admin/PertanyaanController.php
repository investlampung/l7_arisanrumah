<?php

namespace App\Http\Controllers\Web\Admin;

use App\Gambar;
use App\History;
use App\Http\Controllers\Controller;
use App\Http\Requests\TanyaRequest;
use App\Pertanyaan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PertanyaanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $gambar = Gambar::all();
        $no = 1;
        $data = Pertanyaan::orderBy('id', 'DESC')->get()->all();

        return view('admin.pertanyaan.beranda', compact('no', 'data','gambar'));
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TanyaRequest $request)
    {
        $data = $request->all();

        Pertanyaan::create($data);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Data Pertanyaan " . $request->tanya;
        History::create($histori);

        $notification = array(
            'message' => 'Data Pertanyaan "' . $request->tanya . '" berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->route('pertanyaan.index')->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function edit(Pertanyaan $pertanyaan)
    {
        $gambar = Gambar::all();
        return view('admin.pertanyaan.edit', compact('pertanyaan','gambar'));
    }

    public function update(Request $request, $id)
    {
        $tanya = Pertanyaan::findOrFail($id);
        $data = $request->all();
        $tanya->update($data);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Pertanyaan " . $tanya->tanya . " berhasil diubah.";
        History::create($histori);

        $notification = array(
            'message' => 'Pertanyaan "' . $tanya->tanya . '" berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('pertanyaan.index')->with($notification);
    }

    public function destroy($id)
    {
        //
    }
}
