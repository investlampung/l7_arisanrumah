<?php

namespace App\Http\Controllers\Web\Admin;

use App\Gambar;
use App\History;
use App\Http\Controllers\Controller;
use App\Http\Requests\VideoRequest;
use App\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VideoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $gambar = Gambar::all();
        $data2 = Video::where('id', 'not like', 1)->get()->all();
        $data1 = Video::where('id', 1)->get()->first();
        return view('admin.video.beranda', compact('data1', 'data2', 'gambar'));
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VideoRequest $request)
    {
        $data = $request->only('video_key', 'video_value');

        Video::create($data);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Tambah";
        $histori['desc_info'] = "Data Video " . $request->video_key;
        History::create($histori);

        $notification = array(
            'message' => 'Data Video "' . $request->video_key . '" berhasil ditambah.',
            'alert-type' => 'info'
        );

        return redirect()->route('video.index')->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Video $video)
    {
        $gambar = Gambar::all();
        return view('admin.video.edit', compact('video', 'gambar'));
    }

    public function update(Request $request, Video $video)
    {
        $video->update($request->all());

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Data Video " . $video->video_key;
        History::create($histori);

        $notification = array(
            'message' => 'Data Video "' . $video->video_key . '" berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('video.index')->with($notification);
    }

    public function destroy($id)
    {
        $video = Video::findOrFail($id);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Hapus";
        $histori['desc_info'] = "Data Video " . $video->video_key;
        History::create($histori);

        $notification = array(
            'message' => 'Data Video ' . $video->video_key . ' berhasil dihapus.',
            'alert-type' => 'error'
        );

        $video->delete();

        return redirect()->route('video.index')->with($notification);
    }
}
