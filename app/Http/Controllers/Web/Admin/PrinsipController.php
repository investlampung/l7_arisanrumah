<?php

namespace App\Http\Controllers\Web\Admin;

use App\Gambar;
use App\History;
use App\Http\Controllers\Controller;
use App\Prinsip;
use Illuminate\Http\Request;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class PrinsipController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $gambar = Gambar::all();
        $data = Prinsip::all();
        return view('admin.prinsip.beranda', compact('data','gambar'));
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function edit(Prinsip $prinsip)
    {
        $gambar = Gambar::all();
        return view('admin.prinsip.edit', compact('prinsip','gambar'));
    }

    public function update(Request $request, $id)
    {
        $prinsip = Prinsip::findOrFail($id);

        if ($request->hasFile('photo')) {
            $this->deletePhoto($prinsip->photo);
            $data['photo'] = $this->savePhoto($request->file('photo'));
        }

        $prinsip->update($data);

        // HISTORI
        $histori['user'] = Auth::user()->name;
        $histori['info'] = "Ubah";
        $histori['desc_info'] = "Gambar " . $prinsip->judul . " berhasil diubah ";
        History::create($histori);

        $notification = array(
            'message' => 'Gambar "' . $prinsip->judul . '" berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('prinsip.index')->with($notification);
    }

    public function destroy($id)
    {
        //
    }

    protected function savePhoto(UploadedFile $photo)
    {
        $fileName = Str::random(10) . '.' . $photo->guessClientExtension();
        $destinationPath = 'itlabil/images/prinsip';
        $photo->move($destinationPath, $fileName);
        return $fileName;
    }

    public function deletePhoto($filename)
    {
        $path = 'itlabil/images/prinsip/' . $filename;
        return File::delete($path);
    }
}
