<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TanyaRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'tanya' => 'required',
            'jawab' => 'required'
        ];
    }
    public function messages()
    {
        return [

            'tanya.required' => 'Tidak boleh kosong',
            'jawab.required' => 'Tidak boleh kosong'
        ];
    }
}
