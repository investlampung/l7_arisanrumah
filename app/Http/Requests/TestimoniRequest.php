<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TestimoniRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'nama' => 'required',
            'jabatan' => 'required',
            'photo' => 'mimes:jpeg,jpg,png,gif|required|max:10000' // max 10000kb
        ];
    }
    public function messages()
    {
        return [

            'nama.required' => 'Tidak boleh kosong',
            'jabatan.required' => 'Tidak boleh kosong',
            'photo.required' => 'Tidak boleh kosong',
            'photo.mimes' => 'Ekstensi tidak cocok',
            'photo.max' => 'Size Photo Max 1Mb'
        ];
    }
}
