<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TimRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'wilayah' => 'required',
            'nama' => 'required',
            'jabatan' => 'required',
            'telp' => 'required',
            'email' => 'required',
            'photo' => 'mimes:jpeg,jpg,png,gif|max:10000' // max 10000kb
        ];
    }
    public function messages()
    {
        return [
            'wilayah.required' => 'Tidak boleh kosong',
            'nama.required' => 'Tidak boleh kosong',
            'jabatan.required' => 'Tidak boleh kosong',
            'telp.required' => 'Tidak boleh kosong',
            'email.required' => 'Tidak boleh kosong',
            'photo.mimes' => 'Ekstensi tidak cocok',
            'photo.max' => 'Size Photo Max 1Mb'
        ];
    }
}
