<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VideoRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        switch ($this->method()) {
            case 'POST': {
                    return [
                        'video_key'                      => 'required',
                        'video_value'                    => 'required'
                    ];
                }

            case 'PUT':
            case 'PATCH': {
                    return [
                        'video_key'                      => 'required',
                        'video_value'                    => 'required'
                    ];
                }

            default:
                break;
        }
    }
    public function messages()
    {
        return [

            'video_key.required' => 'Tidak boleh kosong',
            'video_value.required' => 'Tidak boleh kosong'
        ];
    }
}
