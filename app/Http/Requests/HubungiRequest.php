<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HubungiRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'nama' => 'required',
            'email' => 'required',
            'telp' => 'required',
            'pesan' => 'required',
            'nik' => 'required'
        ];
    }
    public function messages()
    {
        return [

            'nama.required' => 'Nama tidak boleh kosong',
            'email.required' => 'Email tidak boleh kosong',
            'telp.required' => 'Telpon tidak boleh kosong',
            'pesan.required' => 'Kota tidak boleh kosong',
            'nik.required' => 'NIK tidak boleh kosong',
        ];
    }
}
