@extends('layouts.user.app')

@section('content')
<!-- Kontak -->
<section id="team" data-stellar-background-ratio="2.5">
  <div class="container">
    <div class="row">

      <div class="col-md-12 col-sm-12">
        <!-- SECTION TITLE -->
        <div class="section-title wow fadeInUp" data-wow-delay="0.1s" align="center">
          <h2>Laporan Bulanan</h2>
        </div>
      </div>

      <div class="col-md-12 col-sm-12">
        <div class="news-thumb wow fadeInUp" data-wow-delay="0.4s">
          <div class="news-info">
            Konten Laporan Bulanan
          </div>
        </div>
      </div>

    </div>
  </div>
</section>
@endsection