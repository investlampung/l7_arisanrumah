@extends('layouts.user.app')

@section('content')
<!-- Gallery -->
<section id="news" data-stellar-background-ratio="2.5">
  <div class="container">
    <div class="row">

      <div class="col-md-12 col-sm-12">
        <!-- SECTION TITLE -->
        <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
          <h2>Galeri Video</h2>
        </div>
      </div>
      @foreach($video as $vid)
      <div class="col-md-4 jarak-bawah" style="margin-bottom: 30px;">
        <iframe src="{{$vid->video_value}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
      @endforeach
    </div>
    <div class="row" style="margin-top: 20px;">
      {{ $video->links() }}
    </div>
  </div>
</section>
@endsection