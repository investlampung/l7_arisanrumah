@extends('layouts.user.app')

@section('content')
<!-- Kontak Fix -->
<section id="team" data-stellar-background-ratio="2.5">
  <div class="container">
    <div class="row">

      <div class="col-md-12 col-sm-12">
        <!-- SECTION TITLE -->
        <div class="section-title wow fadeInUp" data-wow-delay="0.1s" align="center">
          <h2>Mitra Griya Indonesia</h2>
        </div>
      </div>

      <div class="col-md-12 col-sm-12">
        <div class="news-thumb wow fadeInUp" data-wow-delay="0.4s">
          <div class="news-info">
            <table class="tabel_kontak">
              <tr>
                <td width="30px"><i class="fa fa-map-marker"></i><br><br></td>
                <td>
                  @foreach($profil as $prof)
                  @if($prof->id===3)
                  {!! $prof->profil_value !!},
                  @endif
                  @endforeach
                  @foreach($profil as $prof)
                  @if($prof->id===2)
                  {!! $prof->profil_value !!}
                  @endif
                  @endforeach
                  <br><br>
                </td>
              </tr>
              <tr>
                <td><i class="fa fa-phone"></i><br><br></td>
                <td>
                  @foreach($profil as $prof)
                  @if($prof->id===6)
                  {!! $prof->profil_value !!}
                  @endif
                  @endforeach
                  <br>
                  @foreach($profil as $prof)
                  @if($prof->id===11)
                  {!! $prof->profil_value !!}
                  @endif
                  @endforeach
                  <br><br>
                </td>
              </tr>
              <tr>
                <td><i class="fa fa-envelope"></i><br><br></td>
                <td>

                  @foreach($profil as $prof)
                  @if($prof->id===5)
                  {!! $prof->profil_value !!}
                  @endif
                  @endforeach
                  <br><br>
                </td>
              </tr>
              <tr>
                <td><i class="fa fa-clock-o"></i><br><br></td>
                <td>

                  @foreach($profil as $prof)
                  @if($prof->id===4)
                  {!! $prof->profil_value !!}
                  @endif
                  @endforeach
                  <br><br>
                </td>
              </tr>
            </table>
            <br>

            @foreach($profil as $prof)
            @if($prof->profil_key==='Facebook')
            <a href="{{ $prof->profil_value }}" class="icon-sosmed facebook"><i class="fa fa-facebook"></i></a>
            @endif
            @endforeach
            @foreach($profil as $prof)
            @if($prof->profil_key==='Instagram')
            <a href="{{ $prof->profil_value }}" class="icon-sosmed instagram"><i class="fa fa-instagram"></i></a>
            @endif
            @endforeach
            @foreach($profil as $prof)
            @if($prof->profil_key==='Whatsapp')
            <a href="{{ $prof->profil_value }}" class="icon-sosmed whatsapp"><i class="fa fa-whatsapp"></i></a>
            @endif
            @endforeach
          </div>
        </div>
      </div>

    </div>
  </div>
</section>

<!-- Tim Marketing Fix -->
<section id="news" data-stellar-background-ratio="2.5">
  <div class="container">
    <div class="row">

      <div class="col-md-12 col-sm-12">
        <!-- SECTION TITLE -->
        <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
          <h2>Tim Marketing</h2>
        </div>
      </div>

      @foreach($tim as $team)
      <div class="col-md-3 col-sm-6">
        <div class="news-thumb wow fadeInUp" data-wow-delay="0.4s">
          <a href="news-detail.html">
            <img src="{{asset('itlabil/images/tm/')}}/{{$team->photo}}" class="img-responsive" alt="" width="300px">
          </a>
          <div class="news-info" style="height: 220px;">
            <h3><a href="news-detail.html">{{$team->wilayah}}</a></h3>
            <table class="tabel_tm">
              <tr>
                <td colspan="2"><b>{{$team->nama}}</b></td>
              </tr>
              <tr>
                <td colspan="2">{{$team->jabatan}}</td>
              </tr>
              <tr>
                <td width="20px"><i class="fa fa-phone"></i></td>
                <td>{{$team->telp}}</td>
              </tr>
              <tr>
                <td><i class="fa fa-envelope"></i></td>
                <td>{{$team->email}}</td>
              </tr>
            </table>
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</section>
@endsection