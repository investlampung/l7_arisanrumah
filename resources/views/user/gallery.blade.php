@extends('layouts.user.app')

@section('content')
<!-- Gallery -->
<section id="news" data-stellar-background-ratio="2.5">
  <div class="container">
    <div class="tz-gallery">
      <div class="row">

        <div class="col-md-12 col-sm-12">
          <!-- SECTION TITLE -->
          <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
            <h2>Galeri Foto</h2>
          </div>
        </div>
        @foreach($photo as $poto)
        <div class="col-md-3 col-sm-6 jarak-bawah">
          <a class="lightbox" href="{{asset('itlabil/images/photo/')}}/{{$poto->photo}}">
            <img src="{{asset('itlabil/images/photo/')}}/{{$poto->photo}}" class="img-thumbnail" alt="{{ $poto->photo }}" style="width: 242px; height:167px;">
          </a>
        </div>
        @endforeach
      </div>
      <div class="row" style="margin-top: 20px;">
      {{ $photo->links() }}
      </div>
    </div>
  </div>
</section>
@endsection