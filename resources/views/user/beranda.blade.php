@extends('layouts.user.app')

@section('content')

<!-- Prinsip Fix -->
<section id="news" data-stellar-background-ratio="2.5">
  <div class="row">
    <div class="col-md-12" style="margin-bottom: 20px;">
      <iframe class="video-tunggall" src="{{$viddd->video_value}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
  </div>
</section>

<!-- Tentang Kami Fix -->
<section id="team" data-stellar-background-ratio="1">
  <div class="container">
    <div class="row">

      <div class="col-md-6 col-sm-6 ">
        <div class="about-info">
          <h2 class="wow fadeInUp" data-wow-delay="0.1s">TENTANG KAMI</h2>
        </div>
      </div>

      <div class="clearfix"></div>

      <div class="col-md-12 team-thumb">

        <div class="col-md-3 col-sm-3">
          <div class="wow fadeInUp" data-wow-delay="0.2s">

            <div class="team-infos" style="color: #ffffff;">
              <h3><i class="fa fa-download"></i> Katalog</h3>
              @foreach($beranda as $ber)
              @if($ber->id===4)
              {!!$ber->beranda_value!!}
              @endif
              @endforeach
              <a href="{{ asset('download/katalog') }}" class="btn btn-default">
                Download Katalog
              </a>
            </div>

          </div>
        </div>

        <div class="col-md-3 col-sm-3">
          <div class="wow fadeInUp" data-wow-delay="0.2s">

            <div class="team-infos">
              <h3><i class="fa fa-bank"></i> Usaha</h3>
              @foreach($beranda as $ber)
              @if($ber->id===7)
              {!!$ber->beranda_value!!}
              @endif
              @endforeach
            </div>

          </div>
        </div>

        <div class="col-md-3 col-sm-3">
          <div class="wow fadeInUp" data-wow-delay="0.6s">

            <div class="team-infos">
              <h3><i class="fa fa-vcard"></i> NPWP</h3>
              @foreach($beranda as $ber)
              @if($ber->id===6)
              {!!$ber->beranda_value!!}
              @endif
              @endforeach
            </div>
          </div>
        </div>

        <div class="col-md-3 col-sm-3">
          <div class="wow fadeInUp" data-wow-delay="0.4s">

            <div class="team-infos">
              <h3><i class="fa fa-download"></i> Rekening</h3>
              @foreach($beranda as $ber)
              @if($ber->id===5)
              {!!$ber->beranda_value!!}
              @endif
              @endforeach
            </div>

          </div>
        </div>

      </div>
    </div>
  </div>
</section>

<!-- Prinsip Fix -->
<section id="news" data-stellar-background-ratio="2.5">
  <div class="container">
    <div class="row">

      <div class="col-md-12 col-sm-12">
        <!-- SECTION TITLE -->
        <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
          <h2>PRINSIP RUMAH ARISAN</h2>
        </div>
      </div>

      @foreach($prinsip as $prin)
      <div class="col-md-4 col-sm-6" style="margin-bottom: 20px;">
        <img src="{{asset('itlabil/images/prinsip/')}}/{{$prin->photo}}" class="img-thumbnail" alt="">
      </div>
      @endforeach
    </div>
  </div>
</section>

<!-- Hubungi Kami Fix -->
<!-- <section id="huuub" class="hubungi" data-stellar-background-ratio="1" style="padding-bottom: 20px;">
  <div class="container">
    <div class="row">

      <div class="col-md-12 col-sm-12">
        <div class="hub-text">
          <h3 class="wow fadeInUp" data-wow-delay="0.1s">Ingin Memiliki Rumah Berkualitas Cash
            / Arisan Dengan Uang Hanya 30 Juta.</h3><br>
          <a href="#appointment" class="section-btn btn btn-white text-black">Hubungi Kami</a>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</section> -->

<!-- Cara Bergabung Fix -->
<section id="news" data-stellar-background-ratio="2.5">
  <div class="container">
    <div class="row">

      <div class="col-md-12 col-sm-12">
        <!-- SECTION TITLE -->
        <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
          <h2>CARA BERGABUNG</h2>
        </div>
      </div>

      <div class="col-md-12 col-sm-12">
        <div class="news-thumb wow fadeInUp" data-wow-delay="0.4s">
          <div class="news-info">
            <h3>Langkah - Langkah nya Seperti Berikut :</h3>
            @foreach($beranda as $ber)
            @if($ber->id===8)
            {!!$ber->beranda_value!!}
            @endif
            @endforeach
          </div>
        </div>
      </div>

    </div>
  </div>
</section>

<!-- Paket Fix -->
<section id="team" data-stellar-background-ratio="1">
  <div class="container">
    <div class="row">

      <div class="col-md-12 col-sm-12">
        <!-- SECTION TITLE -->
        <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
          <center>
            <h2>SPESIFIKASI BANGUNAN</h2>
          </center>
        </div>
      </div>

      <div class="col-md-12  team-thumb">

        <div class="col-md-6 col-sm-6 jarak-bawah">
          <div class="team-thumb wow fadeInUp" data-wow-delay="0.2s">

            <div class="team-info">
              @foreach($beranda as $ber)
              @if($ber->id===9)
              {!!$ber->beranda_value!!}
              @endif
              @endforeach


              <div class="team-contact-info"></div>
            </div>

          </div>
        </div>
        <div class="col-md-6 col-sm-6 jarak-bawah">
          @foreach($gambar as $image)
          @if($image->gambar_key==='Paket 1')
          <img class="img-thumbnail" src="{{asset('itlabil/images/slide/'.$image->gambar_value)}}" alt="" style="margin:20px 20px;">
          @endif
          @endforeach
        </div>
        <!-- <div class="col-md-4 col-sm-6 jarak-bawah">
          <div class="team-thumb wow fadeInUp" data-wow-delay="0.2s">

            <div class="team-info">
              @foreach($beranda as $ber)
              @if($ber->id===10)
              {!!$ber->beranda_value!!}
              @endif
              @endforeach

              @foreach($gambar as $image)
              @if($image->gambar_key==='Paket 2')
              <img class="img-responsive" src="{{asset('itlabil/images/slide/'.$image->gambar_value)}}" alt="">
              @endif
              @endforeach
              <div class="team-contact-info"></div>
            </div>

          </div>
        </div>

        <div class="col-md-4 col-sm-6 jarak-bawah">
          <div class="team-thumb wow fadeInUp" data-wow-delay="0.2s">

            <div class="team-info">
              @foreach($beranda as $ber)
              @if($ber->id===11)
              {!!$ber->beranda_value!!}
              @endif
              @endforeach

              @foreach($gambar as $image)
              @if($image->gambar_key==='Paket 3')
              <img class="img-responsive" src="{{asset('itlabil/images/slide/'.$image->gambar_value)}}" alt="">
              @endif
              @endforeach
              <div class="team-contact-info"></div>
            </div>

          </div>
        </div> -->
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</section>

<!-- Siapa Kami Fix -->
<section id="news" data-stellar-background-ratio="2.5">
  <div class="container">
    <div class="row">

      <div class="col-md-12 col-sm-12">
        <!-- SECTION TITLE -->
        <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
          <h2>SIAPAKAH KAMI</h2>
        </div>
      </div>

      <div class="col-md-12 col-sm-12">
        <div class="news-thumb wow fadeInUp" data-wow-delay="0.4s">
          <div class="news-info">
            @foreach($beranda as $ber)
            @if($ber->id===12)
            {!!$ber->beranda_value!!}
            @endif
            @endforeach
            @foreach($profil as $prof)
            @if($prof->profil_key==='Facebook')
            <a href="{{ $prof->profil_value }}" class="icon-sosmed facebook"><i class="fa fa-facebook"></i></a>
            @endif
            @endforeach
            @foreach($profil as $prof)
            @if($prof->profil_key==='Instagram')
            <a href="{{ $prof->profil_value }}" class="icon-sosmed instagram"><i class="fa fa-instagram"></i></a>
            @endif
            @endforeach
            @foreach($profil as $prof)
            @if($prof->profil_key==='Whatsapp')
            <a href="{{ $prof->profil_value }}" class="icon-sosmed whatsapp"><i class="fa fa-whatsapp"></i></a>
            @endif
            @endforeach
          </div>
        </div>
      </div>

    </div>
  </div>
</section>

<!-- Hubungi Kami Fix -->
<!-- <section id="huuub" class="hubungi" data-stellar-background-ratio="1">
  <div class="container">
    <div class="row">

      <div class="col-md-12 col-sm-12">
        <div class="hub-text">
          <h3 class="wow fadeInUp" data-wow-delay="0.1s">Ingin Memiliki Rumah Berkualitas Cash
            / Arisan Dengan Uang Hanya 30 Juta.</h3><br>
          <a href="#appointment" class="section-btn btn btn-white text-black">Hubungi Kami</a>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</section> -->

<!-- Progres Fix -->
<section id="news" data-stellar-background-ratio="2.5">
  <div class="container">
    <div class="row">

      <div class="col-md-12 col-sm-12">
        <!-- SECTION TITLE -->
        <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
          <h2>PROGRES RUMAH TERBANGUN</h2>
        </div>
      </div>

      @foreach($progres as $prog)
      <div class="col-md-4 col-sm-6">
        <div class="news-thumb wow fadeInUp" data-wow-delay="0.4s">
          <a>
            <img src="{{asset('itlabil/images/progres/')}}/{{$prog->photo}}" class="img-responsive" alt="" style="width: 360px; height:220px;">
          </a>
          <div class="news-info" style="height: 150px;">
            <h3><a>{{$prog->judul}}</a></h3>
            <p></p>
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</section>

<!-- Kelompok Bina Fix -->
<!-- <section id="team" class="hubungi" data-stellar-background-ratio="1">
  <div class="container">
    <div class="row">

      <div class="col-md-12 col-sm-12">
        <div class="hub-text" align="center">
          <h2 class="wow fadeInUp" data-wow-delay="0.1s" style="color:#ffffff">
            Rumah Arisan dan Kelompok Binaan
          </h2><br>
        </div>
      </div>
      <div class="col-md-3 col-sm-6">
        <div class="hub-text" align="center">
          <h2 class="wow fadeInUp" data-wow-delay="0.1s" style="color:#ffffff">
            365
          </h2>
          <h4 style="color:#ffffff">Hari</h4>
        </div>
      </div>
      <div class="col-md-3 col-sm-6">
        <div class="hub-text" align="center">
          <h2 class="wow fadeInUp" data-wow-delay="0.1s" style="color:#ffffff">
            150
          </h2>
          <h4 style="color:#ffffff">Anggota Arisan</h4>
        </div>
      </div>
      <div class="col-md-3 col-sm-6">
        <div class="hub-text" align="center">
          <h2 class="wow fadeInUp" data-wow-delay="0.1s" style="color:#ffffff">
            30
          </h2>
          <h4 style="color:#ffffff">Rumah Terbangun</h4>
        </div>
      </div>
      <div class="col-md-3 col-sm-6">
        <div class="hub-text" align="center">
          <h2 class="wow fadeInUp" data-wow-delay="0.1s" style="color:#ffffff">
            9
          </h2>
          <h4 style="color:#ffffff">Kelompok Arisan</h4>
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</section> -->

<section id="news" data-stellar-background-ratio="2.5" style="background-color: #a5c422;">
  <div class="container">
    <div class="tz-gallery">

      <div class="row">

        <div class="col-md-12">
          <!-- SECTION TITLE -->
          <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
            <h2 style="color: #ffffff;">TESTIMONI</h2>
          </div>
        </div>

        @foreach($testimoni as $testi)
        <div class="col-md-3 col-sm-6 jarak-bawah">
          <a class="lightbox" href="{{asset('itlabil/images/testimoni/')}}/{{$testi->photo}}">
            <img src="{{asset('itlabil/images/testimoni/')}}/{{$testi->photo}}" class="img-thumbnail" alt="{{ $testi->photo }}">
          </a>
        </div>
        @endforeach
      </div>
    </div>
  </div>
</section>

<!-- Gallery Fix -->
<section id="news" data-stellar-background-ratio="2.5">
  <div class="container">

    <div class="tz-gallery">

      <div class="row">

        <div class="col-md-12 col-sm-12">
          <!-- SECTION TITLE -->
          <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
            <h2>GALERI FOTO</h2>
          </div>
        </div>
        @foreach($photo as $poto)
        <div class="col-md-3 col-sm-6 jarak-bawah">
          <a class="lightbox" href="{{asset('itlabil/images/photo/')}}/{{$poto->photo}}">
            <img src="{{asset('itlabil/images/photo/')}}/{{$poto->photo}}" class="img-thumbnail" alt="{{ $poto->photo }}" style="width: 242px; height:167px;">
          </a>
        </div>
        @endforeach
      </div>
      <div class="row" style="margin-top: 20px;">
        <a href="{{url('/gallery')}}" class="gallery-btn">Lihat Galeri Foto</a>
      </div>
    </div>
  </div>
</section>

<!-- Video Fix -->
<section id="news" data-stellar-background-ratio="2.5">
  <div class="container">

    <div class="row">

      <div class="col-md-12 col-sm-12">
        <!-- SECTION TITLE -->
        <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
          <h2>GALERI VIDEO</h2>
        </div>
      </div>
      @foreach($video as $vid)
      <div class="col-md-4 jarak-bawah">
        <iframe src="{{$vid->video_value}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
      @endforeach
    </div>
    <div class="row" style="margin-top: 40px;margin-bottom: 20px;">
      <a href="{{url('/video')}}" class="gallery-btn">Lihat Gallery Video</a>
    </div>
  </div>
</section>

<!-- Hubungi Kami Fix -->
<section id="appointment" data-stellar-background-ratio="3">
  <div class="container">
    <div class="row">

      <div class="col-md-6 col-sm-6">
        @foreach($gambar as $image)
        @if($image->gambar_key==='Gambar Hubungi')
        <img src="{{asset('itlabil/images/slide/')}}/{{$image->gambar_value}}" class="img-responsive" alt="">
        @endif
        @endforeach
      </div>

      <div class="col-md-6 col-sm-6">
        <!-- CONTACT FORM HERE -->
        <form id="appointment-form" role="form" method="post" action="/pesan/store" enctype="multipart/form-data">
          {{ csrf_field() }}
          <!-- SECTION TITLE -->
          <div class="section-title wow fadeInUp" data-wow-delay="0.4s">
            <h2>HUBUNGI KAMI</h2>
          </div>

          <div class="wow fadeInUp" data-wow-delay="0.8s">
            <div class="col-md-6 col-sm-6">
              <label for="name">Nama</label>
              <input type="text" class="form-control" id="name" name="nama" placeholder="Nama Lengkap">
              <small class="text-danger">{{ $errors->first('nama') }}</small>
            </div>

            <div class="col-md-6 col-sm-6">
              <label for="nik">NIK</label>
              <input type="nik" class="form-control" id="nik" name="nik" placeholder="NIK">
              <small class="text-danger">{{ $errors->first('nik') }}</small>
            </div>

            <div class="col-md-6 col-sm-6">
              <label for="email">Email</label>
              <input type="email" class="form-control" id="email" name="email" placeholder="Email">
              <small class="text-danger">{{ $errors->first('email') }}</small>
            </div>

            <div class="col-md-6 col-sm-6">
              <label for="telephone">Nomor HP / Whatsapp</label>
              <input type="tel" class="form-control" id="phone" name="telp" placeholder="Telepon / HP">
              <small class="text-danger">{{ $errors->first('telp') }}</small>
            </div>

            <div class="col-md-6 col-sm-6">
              <label for="Message">Kota</label>
              <input type="text" class="form-control" id="pesan" name="pesan" placeholder="Kota">
              <small class="text-danger">{{ $errors->first('pesan') }}</small>
            </div>
            
            <div class="col-md-6 col-sm-6">
              <label for="marketing">Rekomendasi Marketing</label>
              <input type="text" class="form-control" id="marketing" name="marketing" placeholder="Rekomendasi Marketing">
              <small class="text-danger">{{ $errors->first('marketing') }}</small>
            </div>

            <div class="col-md-12 col-sm-12">
              <label for="photo">Foto</label>
              <input type="file" name="photo" id="cat_image">
              <small class="text-danger">{{ $errors->first('photo') }}</small>
              <button type="submit" class="form-control" id="cf-submit" name="submit">Kirim</button>
            </div>
          </div>
        </form>
      </div>

    </div>
  </div>
</section>

<!-- Pertanyaan Fix -->
<section id="news" data-stellar-background-ratio="2.5">
  <div class="container">
    <div class="row">

      <div class="col-md-12 col-sm-12">
        <!-- SECTION TITLE -->
        <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
          <h2>PERTANYAAN SEPUTAR RUMAH ARISAN</h2>
        </div>
      </div>

      <div class="col-md-12 col-sm-12 pertanyaan">
        @foreach($tanya as $ask)
        <p>
          <a data-toggle="collapse" href="#collapseExample{{$ask->id}}" role="button" aria-expanded="false" aria-controls="collapseExample{{$ask->id}}">
            {!!$ask->tanya!!}
          </a>
        </p>
        <div class="collapse" id="collapseExample{{$ask->id}}">
          <div class="card card-body">
            {!!$ask->jawab!!}
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </div>
</section>

<!-- GOOGLE MAP -->
<section id="google-map">
  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3972.2179982711277!2d105.06653665004032!3d-5.3837037960758725!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e40d2858ffc6895%3A0x153a3fe8fc5bad70!2sMitra%20Griya%20Indonesia!5e0!3m2!1sen!2sid!4v1598686080039!5m2!1sen!2sid" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
</section>
@endsection