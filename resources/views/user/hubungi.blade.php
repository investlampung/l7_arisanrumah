@extends('layouts.user.hubungi')

@section('content')
<!-- Hubungi Kami Fix -->
<section id="appointment" data-stellar-background-ratio="3">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12" align="center">
        @foreach($gambar as $image)
        @if($image->gambar_key==='Logo')
        <img src="{{asset('itlabil/images/slide/'.$image->gambar_value)}}" alt="" class="img-responsive" width="250px">
        @endif
        @endforeach
        <br><br>
      </div>
      <div class="col-md-12 col-sm-12">
        <!-- CONTACT FORM HERE -->
        <form id="appointment-form" role="form" method="post" action="/pesan/store" enctype="multipart/form-data">
          {{ csrf_field() }}
          <!-- SECTION TITLE -->
          <div class="section-title wow fadeInUp" data-wow-delay="0.4s" align="center">
            <h2>Hubungi Kami</h2>
          </div>

          <div class="wow fadeInUp" data-wow-delay="0.8s">
            <div class="col-md-12 col-sm-12">
              <label for="name">Nama</label>
              <input type="text" class="form-control" id="name" name="nama" placeholder="Nama Lengkap">
              <small class="text-danger">{{ $errors->first('nama') }}</small>
            </div>

            <div class="col-md-12 col-sm-12">
              <label for="nik">NIK</label>
              <input type="text" class="form-control" id="nik" name="nik" placeholder="Nomor Induk Kependudukan">
              <small class="text-danger">{{ $errors->first('nik') }}</small>
            </div>

            <div class="col-md-12 col-sm-12">
              <label for="email">Email</label>
              <input type="email" class="form-control" id="email" name="email" placeholder="Email">
              <small class="text-danger">{{ $errors->first('email') }}</small>
            </div>

            <div class="col-md-12 col-sm-12">
              <label for="telephone">Nomor HP / Whatsapp</label>
              <input type="tel" class="form-control" id="phone" name="telp" placeholder="Telepon / HP">
              <small class="text-danger">{{ $errors->first('telp') }}</small>
            </div>

            <div class="col-md-12 col-sm-12">
              <label for="Message">Kota</label>
              <input type="text" class="form-control" id="pesan" name="pesan" placeholder="Kota">
              <small class="text-danger">{{ $errors->first('pesan') }}</small>
            </div>

            <div class="col-md-12 col-sm-12">
              <label for="marketing">Rekomendasi Marketing</label>
              <input type="text" class="form-control" id="marketing" name="marketing" placeholder="Rekomendasi Marketing">
              <small class="text-danger">{{ $errors->first('marketing') }}</small>
            </div>

            <div class="col-md-12 col-sm-12">
              <label for="photo">Foto</label>
              <input type="file" name="photo" id="cat_image">
              <small class="text-danger">{{ $errors->first('photo') }}</small>
            </div>

            <div class="col-md-12 col-sm-12">
              <button type="submit" class="form-control" id="cf-submit" name="submit">Kirim</button>
            </div>
          </div>
        </form>
      </div>

    </div>
  </div>
</section>
@endsection