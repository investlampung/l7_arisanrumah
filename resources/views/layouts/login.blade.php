<!DOCTYPE html>
<html lang="en">

<head>
	<title>Rumah Arisan Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="{{asset('itlabil/login/images/logo.jpg')}}" />
	<link rel="stylesheet" type="text/css" href="{{ asset('itlabil/login/vendor/bootstrap/css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('itlabil/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('itlabil/login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('itlabil/login/vendor/animate/animate.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('itlabil/login/vendor/css-hamburgers/hamburgers.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('itlabil/login/vendor/animsition/css/animsition.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('itlabil/login/vendor/select2/select2.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('itlabil/login/vendor/daterangepicker/daterangepicker.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('itlabil/login/css/util.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('itlabil/login/css/main.css') }}">
</head>

<body>
	@yield('content')


	<script src="{{ asset('itlabil/login/vendor/jquery/jquery-3.2.1.min.js') }}"></script>
	<script src="{{ asset('itlabil/login/vendor/animsition/js/animsition.min.js') }}"></script>
	<script src="{{ asset('itlabil/login/vendor/bootstrap/js/popper.js') }}"></script>
	<script src="{{ asset('itlabil/login/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('itlabil/login/vendor/select2/select2.min.js') }}"></script>
	<script src="{{ asset('itlabil/login/vendor/daterangepicker/moment.min.js') }}"></script>
	<script src="{{ asset('itlabil/login/vendor/daterangepicker/daterangepicker.js') }}"></script>
	<script src="{{ asset('itlabil/login/vendor/countdowntime/countdowntime.js') }}"></script>
	<script src="{{ asset('itlabil/login/js/main.js"></script>

</body>
</html>