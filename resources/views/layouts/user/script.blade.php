  <!-- SCRIPTS -->
  <script src="{{asset('itlabil/user/js/jquery.js')}}"></script>
  <script src="{{asset('itlabil/user/js/bootstrap.min.js')}}"></script>
  <script src="{{asset('itlabil/user/js/jquery.sticky.js')}}"></script>
  <script src="{{asset('itlabil/user/js/jquery.stellar.min.js')}}"></script>
  <script src="{{asset('itlabil/user/js/wow.min.js')}}"></script>
  <script src="{{asset('itlabil/user/js/smoothscroll.js')}}"></script>
  <script src="{{asset('itlabil/user/js/owl.carousel.min.js')}}"></script>
  <script src="{{asset('itlabil/user/js/custom.js')}}"></script>
  <script>
    function myFunction(id) {
      var x = document.getElementById(id);
      if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
      } else {
        x.className = x.className.replace(" w3-show", "");
      }
    }
  </script>
  <script type="text/javascript" src="{{ asset('itlabil/admin/toast/toastr.min.js') }}"></script>

  <script>
    @if(Session::has('message'))
    var type = "{{Session::get('alert-type','info')}}"

    switch (type) {
      case 'info':
        toastr.info("{{ Session::get('message') }}");
        break;
      case 'success':
        toastr.success("{{ Session::get('message') }}");
        break;
      case 'warning':
        toastr.warning("{{ Session::get('message') }}");
        break;
      case 'error':
        toastr.error("{{ Session::get('message') }}");
        break;
    }
    @endif
  </script>
  <script src="{{ asset('itlabil/user/js/baguetteBox.min.js') }}"></script>

  <script>
    baguetteBox.run('.tz-gallery');
  </script>