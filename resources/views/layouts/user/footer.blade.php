<!-- FOOTER -->
<footer data-stellar-background-ratio="5">
  <div class="container">
    <div class="row">

      <div class="col-md-6 col-sm-6">
        <div class="footer-thumb">
          <h4 class="wow fadeInUp" data-wow-delay="0.4s">Kontak</h4>
          <p>
            @foreach($profil as $prof)
            @if($prof->id===3)
            {!!$prof->profil_value!!},
            @endif
            @endforeach
            @foreach($profil as $prof)
            @if($prof->id===2)
            {!!$prof->profil_value!!}
            @endif
            @endforeach
          </p>

          <div class="contact-info">
            <p><i class="fa fa-phone"></i>

              @foreach($profil as $prof)
              @if($prof->id===6)
              {!!$prof->profil_value!!}
              @endif
              @endforeach
            </p>
            <p><i class="fa fa-phone"></i>

              @foreach($profil as $prof)
              @if($prof->id===11)
              {!!$prof->profil_value!!}
              @endif
              @endforeach
            </p>
            <p><i class="fa fa-envelope-o"></i>
              <a href="#">
                @foreach($profil as $prof)
                @if($prof->id===5)
                {!!$prof->profil_value!!}
                @endif
                @endforeach
              </a>
            </p>
          </div>
        </div>
      </div>

      <div class="col-md-6 col-sm-6">
        <div class="footer-thumb">
          <div class="opening-hours">
            <h4 class="wow fadeInUp" data-wow-delay="0.4s">Jam Kerja</h4>
            <p>

              @foreach($profil as $prof)
              @if($prof->id===4)
              {!!$prof->profil_value!!}
              @endif
              @endforeach
            </p>
          </div>

          <ul class="social-icon">

            @foreach($profil as $prof)
            @if($prof->profil_key==='Facebook')
            <li><a href="{{$prof->profil_value}}" class="fa fa-facebook-square" attr="facebook icon"></a></li>
            @endif
            @endforeach
            @foreach($profil as $prof)
            @if($prof->profil_key==='Instagram')
            <li><a href="{{$prof->profil_value}}" class="fa fa-instagram" attr="instagram icon"></a></li>
            @endif
            @endforeach
          </ul>
        </div>
      </div>

      <div class="col-md-12 col-sm-12 border-top">
        <div class="col-md-4 col-sm-6">
          <div class="copyright-text">
            <p>Copyright &copy; 2020 Rumah Arisan

              | Design : Source Media</p>
          </div>
        </div>
        <div class="col-md-6 col-sm-6">
          <div class="footer-link">
            <a href="{{url('/kontak')}}">Kontak</a>
            <a href="{{url('/laporan')}}">Laporan Bulanan</a>
          </div>
        </div>
        <div class="col-md-2 col-sm-2 text-align-center">
          <div class="angle-up-btn">
            <a href="#top" class="smoothScroll wow fadeInUp" data-wow-delay="1.2s"><i class="fa fa-angle-up"></i></a>
          </div>
        </div>
      </div>

    </div>
  </div>
</footer>

<div class="icon-side-sosmed">

  @foreach($profil as $prof)
  @if($prof->profil_key==='Facebook')
  <a href="{{$prof->profil_value}}" class="facebook"><i class="fa fa-facebook"></i></a>
  @endif
  @endforeach
  @foreach($profil as $prof)
  @if($prof->profil_key==='Instagram')
  <a href="{{$prof->profil_value}}" class="instagram"><i class="fa fa-instagram"></i></a>
  @endif
  @endforeach
  @foreach($profil as $prof)
  @if($prof->profil_key==='Whatsapp')
  <a href="{{$prof->profil_value}}" class="whatsapp"><i class="fa fa-whatsapp"></i></a>
  @endif
  @endforeach
  @foreach($profil as $prof)
  @if($prof->profil_key==='Youtube')
  <a href="{{$prof->profil_value}}" class="youtube"><i class="fa fa-youtube"></i></a>
  @endif
  @endforeach
</div>