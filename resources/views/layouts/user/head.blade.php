<title>Rumah Arisan</title>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="Source Media">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
@foreach($gambar as $image)
@if($image->gambar_key==='Logo')
<link rel="shortcut icon" href="{{asset('itlabil/images/slide/'.$image->gambar_value)}}">
@endif
@endforeach

<link rel="stylesheet" href="{{asset('itlabil/user/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{asset('itlabil/user/css/font-awesome.min.css')}}">
<link rel="stylesheet" href="{{asset('itlabil/user/css/animate.css')}}">
<link rel="stylesheet" href="{{asset('itlabil/user/css/owl.carousel.css')}}">
<link rel="stylesheet" href="{{asset('itlabil/user/css/owl.theme.default.min.css')}}">
<link rel="stylesheet" href="{{asset('itlabil/user/css/tooplate-style.css')}}">
<link rel="stylesheet" href="{{asset('itlabil/user/css/custom.css')}}">
<link href="{{ asset('itlabil/admin/toast/toastr.min.css') }}" rel="stylesheet">
<link href="{{ asset('itlabil/user/css/gallery-grid.css') }}" rel="stylesheet"> 
<link href="{{ asset('itlabil/user/css/baguetteBox.min.css') }}" rel="stylesheet"> 

<style>
  .slider .item-first {
    @foreach($gambar as $image) 
		@if($image->gambar_key==='Slide 1') 
    background-image: url({{asset('itlabil/images/slide/'.$image->gambar_value)}});
    @endif 
		@endforeach
  }

  .slider .item-second {
    @foreach($gambar as $image) 
		@if($image->gambar_key==='Slide 2') 
    background-image: url({{asset('itlabil/images/slide/'.$image->gambar_value)}});
    @endif 
		@endforeach
  }

  .slider .item-third {
    @foreach($gambar as $image) 
		@if($image->gambar_key==='Slide 3') 
    background-image: url({{asset('itlabil/images/slide/'.$image->gambar_value)}});
    @endif 
		@endforeach
  }
  .slider .item-fourd {
    @foreach($gambar as $image) 
		@if($image->gambar_key==='Slide 4') 
    background-image: url({{asset('itlabil/images/slide/'.$image->gambar_value)}});
    @endif 
		@endforeach
  }
</style>