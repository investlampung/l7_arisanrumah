<!-- HEADER -->
<header>
  <div class="container">
    <div class="row">

      <div class="col-md-4 col-sm-5">
        <p>
          Selamat Datang di Website
          @foreach($profil as $prof)
          @if($prof->id===1)
          {{$prof->profil_value}}
          @endif
          @endforeach
        </p>
      </div>

      <div class="col-md-8 col-sm-7 text-align-right">
        <span class="phone-icon">
          <i class="fa fa-phone"></i>
          @foreach($profil as $prof)
          @if($prof->id===6)
          {{$prof->profil_value}}
          @endif
          @endforeach
        </span>
        <span class="date-icon"><i class="fa fa-calendar-plus-o"></i>
          @foreach($profil as $prof)
          @if($prof->id===4)
          {{$prof->profil_value}}
          @endif
          @endforeach
        </span>
        <span class="email-icon"><i class="fa fa-envelope-o"></i>
          <a href="#">
            @foreach($profil as $prof)
            @if($prof->id===5)
            {{$prof->profil_value}}
            @endif
            @endforeach
          </a>
        </span>
      </div>

    </div>
  </div>
</header>

<!-- MENU -->
<section class="navbar navbar-default navbar-static-top" role="navigation">
  <div class="container">

    <div class="navbar-header">
      <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="icon icon-bar"></span>
        <span class="icon icon-bar"></span>
        <span class="icon icon-bar"></span>
      </button>

      <!-- lOGO TEXT HERE -->
      <a href="{{url('/')}}" class="navbar-brand">
        @foreach($profil as $prof)
        @if($prof->id===1)
        {{$prof->profil_value}}
        @endif
        @endforeach
      </a>
    </div>

    <!-- MENU LINKS -->
    <div class="collapse navbar-collapse">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="{{url('/')}}" class="smoothScroll">Beranda</a></li>
        <li><a href="{{url('/kontak')}}" class="smoothScroll">Kontak</a></li>
        <li><a href="{{url('/laporan')}}" class="smoothScroll">Laporan Bulanan</a></li>
        <!-- <li><a href="#news" class="smoothScroll">News</a></li>
                         <li><a href="#google-map" class="smoothScroll">Contact</a></li> -->
        <li class="appointment-btn"><a href="
          @if($page==='beranda')
            #appointment
          @else
            /#appointment
          @endif
        ">Hubungi Kami</a></li>
      </ul>
    </div>

  </div>
</section>

<!-- Slide -->
<section id="home" class="slider" data-stellar-background-ratio="0.5">
  <div class="container">
    <div class="row">

      <div class="owl-carousel owl-theme">
        <div class="item item-first">
          <div class="caption">
            <div class="col-md-offset-1 col-md-10">
              <!-- <h3>Harga Terjangkau, Berkualitas dan Mudah</h3>
              <h1>Arisan Rumah</h1>
              <h4>Harga Mulai dari :
                @foreach($beranda as $ber)
                @if($ber->id===1)
                {{$ber->beranda_value}}
                @endif
                @endforeach
              </h4>
              <a href="
              @if($page==='beranda')
                #appointment
              @else
               /#appointment
              @endif
              " class="section-btn btn btn-default smoothScroll">Daftar Segera</a> -->
            </div>
          </div>
        </div>

        <div class="item item-second">
          <div class="caption">
            <div class="col-md-offset-1 col-md-10">
              <!-- <h3>Dapatkan Rumah Dengan Konsep Arisan dan Gotong Royong</h3>
              <h1>Arisan Rumah</h1>
              <h4>Bisa Dimiliki Dengan Bulanan :
                @foreach($beranda as $ber)
                @if($ber->id===2)
                {{$ber->beranda_value}}
                @endif
                @endforeach
              </h4>
              <a href="
              @if($page==='beranda')
                #appointment
              @else
               /#appointment
              @endif
              " class="section-btn btn btn-default btn-default smoothScroll">Daftar
                Segera</a> -->
            </div>
          </div>
        </div>

        <div class="item item-third">
          <div class="caption">
            <div class="col-md-offset-1 col-md-10">
              <!-- <h3>Harga Terjangkau, Berkualitas dan Mudah</h3>
              <h1>Arisan Rumah</h1>
              <h4>Harga Mulai dari :
                @foreach($beranda as $ber)
                @if($ber->id===3)
                {{$ber->beranda_value}}
                @endif
                @endforeach
              </h4>
              <a href="
              @if($page==='beranda')
                #appointment
              @else
               /#appointment
              @endif
              " class="section-btn btn btn-default btn-default smoothScroll">Daftar
                Segera</a> -->
            </div>
          </div>
        </div>

        <!-- <div class="item item-fourd">
          <div class="caption">
            <div class="col-md-offset-1 col-md-10">
              <h3>Daftarkan Segera Sebelum Quota HABIS</h3>
              <h1>Arisan Rumah</h1>
              <a href="
              @if($page==='beranda')
                #appointment
              @else
               /#appointment
              @endif
              " class="section-btn btn btn-default btn-default smoothScroll">Daftar
                Segera</a>
            </div>
          </div>
        </div> -->
      </div>

    </div>
  </div>
</section>