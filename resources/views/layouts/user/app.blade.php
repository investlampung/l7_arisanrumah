<!DOCTYPE html>
<html lang="en">

<head>
  @include('layouts.user.head')
</head>

<body id="top" data-spy="scroll" data-target=".navbar-collapse" data-offset="50">

  <!-- PRE LOADER -->
  <section class="preloader">
    <div class="spinner">

      <span class="spinner-rotate"></span>

    </div>
  </section>

  @include('layouts.user.header')

  @yield('content')

  @include('layouts.user.footer')

  @include('layouts.user.script')

</body>

</html>