@extends('layouts.admin')

@section('content')


<div class="container">

  <section class="content-header">
    <h1>
      File
    </h1>
  </section><br><br>

  <div class="row">

    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Ganti File Katalog</h3>
        </div>
        <form action="{{ route('file.store') }}" class="form-horizontal" method="POST" enctype="multipart/form-data">
          @csrf
          <div class="box-body">
            <div class="form-group">
              <label for="photo" class="col-sm-2 control-label">File</label>

              <div class="col-sm-8">
                <input type="file" name="file" id="cat_image">
                <br>
                *File berextensi .pdf
              </div>
            </div>
          </div>
          <div class="box-footer">
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

@endsection