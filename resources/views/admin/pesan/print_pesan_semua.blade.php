<!DOCTYPE html>
<html>

<head>
  <title>Semua Data Klien</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <style>
    hr.new4 {
      border: 2px solid #000000;
    }

    table {
      width: 100%;
    }

    table,
    tr,
    td,
    th {
      padding: 5px 5px;
    }
  </style>
</head>

<body onload="window.print()">
  <div class="container">
    <table width="100%">
      <tbody>
        <tr>
          <td>
            <img src="{{asset('itlabil/images/default/logo.jpg')}}" width="200px">
          </td>
          <td align="center">
            <b>
              <h3>CV. MITRA GRIYA INDONESIA</h3>
            </b>
            @foreach($profil as $prof)
            @if($prof->id===3)
            <h5>{{$prof->profil_value}}</h5>
            @endif
            @endforeach
            @foreach($profil as $prof)
            @if($prof->id===2)
            <h5>{{$prof->profil_value}}</h5>
            @endif
            @endforeach
          </td>
        </tr>
      </tbody>
    </table>
    <hr class="new4">
    <table border="1">
      <thead>
        <tr>
          <th>No</th>
          <th>Nama</th>
          <th>NIK</th>
          <th>Email</th>
          <th>No Hp / Whatsapp</th>
          <th>Kota</th>
          <th>Rekomendasi</th>
        </tr>
      </thead>
      <tbody>
        @php 
          $no=1; 
        @endphp
        @foreach($data as $item)
        <tr>
          <td>{{ $no++ }}</td>
          <td>{{ $item->nama }}</td>
          <td>{{ $item->nik }}</td>
          <td>{{ $item->email }}</td>
          <td>{{ $item->telp }}</td>
          <td>{{ $item->pesan }}</td>
          <td>{{ $item->rekomendasi }}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>

</body>

</html>