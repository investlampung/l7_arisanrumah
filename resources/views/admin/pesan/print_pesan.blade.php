<!DOCTYPE html>
<html>

<head>
  <title>Data Klien - {{$data->nama}}</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <style>
    hr.new4 {
      border: 2px solid #000000;
    }
  </style>
</head>

<body onload="window.print()">
  <div class="container">
    <table width="100%">
      <tbody>
        <tr>
          <td>
            <img src="{{asset('itlabil/images/default/logo.jpg')}}" width="200px">
          </td>
          <td align="center">
            <b>
              <h3>CV. MITRA GRIYA INDONESIA</h3>
            </b>
            @foreach($profil as $item)
            @if($item->id===3)
            <h5>{{$item->profil_value}}</h5>
            @endif
            @endforeach
            @foreach($profil as $item)
            @if($item->id===2)
            <h5>{{$item->profil_value}}</h5>
            @endif
            @endforeach
          </td>
        </tr>
      </tbody>
    </table>
    <hr class="new4">
    <table width="100%">
      <tbody>
        <tr>
          @if(empty($data->photo))
          @else
          <td colspan="2" align="center">
            <img src="{{asset('itlabil/images/pesan/'.$data->photo) }}" class="img-thumbnail" width="300px" alt="">
            <br><br>
          </td>
          @endif
        </tr>
        <tr>
          <td width="200px">Nama</td>
          <td>: {{ $data->nama }}</td>
        </tr>
        <tr>
          <td width="200px">NIK</td>
          <td>: {{ $data->nik }}</td>
        </tr>
        <tr>
          <td>Email</td>
          <td>: {{ $data->email }}</td>
        </tr>
        <tr>
          <td>Nomor HP/Whatsapp</td>
          <td>: {{ $data->telp }}</td>
        </tr>
        <tr>
          <td>Kota</td>
          <td>: {{ $data->pesan }}</td>
        </tr>
        <tr>
          <td width="200px">Rekomendasi Marketing</td>
          <td>: {{ $data->marketing }}</td>
        </tr>
      </tbody>
    </table>
    <hr>
    <table width="100%">
      <tbody>
        <tr>
          <td width="70%"></td>
          <td align="center">Gadingrejo, {{ $data->created_at->format('d-m-Y') }}.</td>
        </tr>
        <tr>
          <td></td>
          <td align="center">Yang Mendaftar</td>
        </tr>
        <tr height="80px">
          <td></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td align="center">{{ $data->nama }}</td>
        </tr>
      </tbody>
    </table>
  </div>

</body>

</html>