@extends('layouts.admin')

@section('content')

<section class="content-header">
  <h1>
    Pesan
  </h1>
</section>

<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Data Pesan</h3>
        </div>
        <div class="box-body" style="overflow-x:auto;">
          <a href="/admin/cetak/pesan-semua/print" target="_blank" class="btn btn-primary">Cetak Semua</a>
        </div>
        <div class="box-body">
          <div class="box-body" style="overflow-x:auto;">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Photo</th>
                  <th>Nama</th>
                  <th>NIK</th>
                  <th>Email</th>
                  <th>Telpon</th>
                  <th>Kota</th>
                  <th>Marketing</th>
                  <th>Tanggal</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach( $data as $item )
                <tr>
                  <td>{{ $no++ }}</td>
                  <td>
                    @if(empty($item->photo))
                    @else
                      <img src="{{asset('itlabil/images/pesan/'.$item->photo) }}" class="img-thumbnail" width="100px" alt="">
                    @endif
                  </td>
                  <td>{{ $item->nama }}</td>
                  <td>{{ $item->nik }}</td>
                  <td>{{ $item->email }}</td>
                  <td>{{ $item->telp }}</td>
                  <td>{{ $item->pesan }}</td>
                  <td>{{ $item->marketing }}</td>
                  <td>{{ $item->created_at->format('d-m-Y, h:m:s') }}</td>
                  <td width="150px">
                    <form action="{{ route('pesan.destroy',$item->id) }}" method="POST">
                      <a href="/admin/cetak/pesan/print/{{$item->id}}" target="_blank" class="btn btn-success">Cetak</a>
                      @csrf
                      @method('DELETE')
                      <button type="submit" class="btn btn-danger">Hapus</button>
                    </form>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection