@extends('layouts.admin')

@section('content')


<div class="container">

    <section class="content-header">
        <h1>
            Gambar
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Gambar</h3>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Judul</th>
                                <th>Isi</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($data as $item)
                            @if($item->gambar_key==='Slide 4')
                            @else
                            <tr>
                                <td>{{ $item->gambar_key }}</td>
                                <td><img src="{{asset('itlabil/images/slide/'.$item->gambar_value) }}" class="img-thumbnail" width="300px" alt=""></td>
                                <td align="center">
                                    <a href="{{ route('gambar.edit', $item->id) }}" class="btn btn-primary">Ubah</a>
                                </td>
                            </tr>
                            @endif
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection