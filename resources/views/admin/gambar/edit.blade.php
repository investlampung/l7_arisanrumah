@extends('layouts.admin')

@section('content')

<div class="container">

  <section class="content-header">
    <h1>
      Data Gambar
    </h1>
  </section><br><br>

  <div class="row">

    <div class="col-md-4">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Edit Data Gambar</h3>
        </div>
        <form action="{{ route('gambar.update',$gam->id) }}" class="form-horizontal" method="POST" enctype="multipart/form-data">
          @csrf
          @method('PATCH')
          <div class="box-body">
            <div class="form-group">
              <label for="{{$gam->gambar_key}}" class="col-sm-4 control-label">{{$gam->gambar_key}}</label>

              <div class="col-sm-8">
                <input type="file" name="gambar_value" id="cat_image">
              </div>
            </div>
          </div>
          <div class="box-footer">
            <a href="{{url('admin/gambar')}}" class="btn btn-default">Kembali</a>
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
          </div>
        </form>
      </div>
    </div>

    <div class="col-md-4">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Gambar Sebelumnya</h3>
        </div>
        <div class="box-body">
          <img src="{{asset('itlabil/images/slide/'.$gam->gambar_value) }}" class="img-thumbnail" width="300px" alt="">
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Gambar Preview</h3>
        </div>
        <div class="box-body">
          <img src="#" id="category-img-tag" class="img-thumbnail" width="300px" />
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
@endsection