@extends('layouts.admin')

@section('content')

<div class="container">

  <section class="content-header">
    <h1>
      Data Testimoni
    </h1>
  </section><br><br>

  <div class="row">

    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Edit Data Testimoni</h3>
        </div>
        <form action="{{ route('testimoni.update',$testimoni->id) }}" class="form-horizontal" method="POST" enctype="multipart/form-data">
          @csrf
          @method('PATCH')
          <div class="box-body">
            <!-- <div class="form-group">
              <label for="nama" class="col-sm-2 control-label">Nama</label>

              <div class="col-sm-10">
                <input type="text" value="{{$testimoni->nama}}" name="nama" class="form-control" placeholder="Nama">
                <small class="text-danger">{{ $errors->first('nama') }}</small>
              </div>
            </div>

            <div class="form-group">
              <label for="jabatan" class="col-sm-2 control-label">Jabatan</label>

              <div class="col-sm-10">
                <input type="text" value="{{$testimoni->jabatan}}" name="jabatan" class="form-control" placeholder="Jabatan">
                <small class="text-danger">{{ $errors->first('jabatan') }}</small>
              </div>
            </div>

            <div class="form-group">
              <label for="testimoni" class="col-sm-2 control-label">Testimoni</label>

              <div class="col-sm-10">
                <div class="box-body pad">
                  <textarea name="testimoni" class="textarea" placeholder="Testimoni" style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px">{{$testimoni->testimoni}}</textarea>
                </div>
                <small class="text-danger">{{ $errors->first('testimoni') }}</small>
              </div>
            </div> -->
            <div class="form-group">
              <label for="Photo" class="col-sm-2 control-label">Photo</label>

              <div class="col-sm-10">
                <input type="file" name="photo" id="cat_image">
              </div>
            </div>
          </div>
          <div class="box-footer">
            <a href="{{url('admin/testimoni')}}" class="btn btn-default">Kembali</a>
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
          </div>
        </form>
      </div>
    </div>

    <div class="col-md-12">
      <div class="col-md-6">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Gambar Sebelumnya</h3>
          </div>
          <div class="box-body">
            <img src="{{asset('itlabil/images/testimoni/'.$testimoni->photo) }}" class="img-thumbnail" width="300px" alt="">
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Gambar Preview</h3>
          </div>
          <div class="box-body">
            <img src="#" id="category-img-tag" class="img-thumbnail" width="300px" />
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
</div>
</div>
@endsection