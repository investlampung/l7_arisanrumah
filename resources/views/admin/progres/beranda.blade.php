@extends('layouts.admin')

@section('content')


<div class="container">

  <section class="content-header">
    <h1>
      Progres
    </h1>
  </section><br><br>

  <div class="row">

    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Tambah Data Progres</h3>
        </div>
        <form action="{{ route('progres.store') }}" class="form-horizontal" method="POST" enctype="multipart/form-data">
          @csrf
          <div class="box-body">

            <div class="form-group">
              <label for="judul" class="col-sm-2 control-label">Judul</label>

              <div class="col-sm-10">
                <input type="text" name="judul" class="form-control" placeholder="Judul">
                <small class="text-danger">{{ $errors->first('judul') }}</small>
              </div>
            </div>

            <div class="form-group">
              <label for="photo" class="col-sm-2 control-label">Photo</label>

              <div class="col-sm-8">
                <input type="file" name="photo" id="cat_image">
              </div>
            </div>
          </div>
          <div class="box-footer">
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
          </div>
        </form>
      </div>
    </div>
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Gambar Preview</h3>
        </div>
        <div class="box-body">
          <img src="#" id="category-img-tag" class="img-thumbnail" width="300px" />
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Data Progres</h3>
        </div>

        <div class="box-body" style="overflow-x:auto;">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th>Judul</th>
                <th>Photo</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>

              @foreach($data as $item)
              <tr>
                <td>{{ $item->judul }}</td>
                <td><img src="{{asset('itlabil/images/progres/'.$item->photo) }}" class="img-thumbnail" width="300px" alt=""></td>
                <td align="center">
                  <form action="{{ route('progres.destroy',$item->id) }}" method="POST">
                    <a href="{{ route('progres.edit', $item->id) }}" class="btn btn-primary">Ubah</a>
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Hapus</button>
                  </form>
                </td>
              </tr>
              @endforeach

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection