@extends('layouts.admin')

@section('content')

<section class="content-header">
  <h1>
    Gallery
  </h1>
</section>

<section class="content">

  <div class="row">

    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Tambah Data Progres</h3>
        </div>
        <form action="{{ route('photo.store') }}" class="form-horizontal" method="POST" enctype="multipart/form-data">
          @csrf
          <div class="box-body">

            <div class="form-group">
              <label for="photo" class="col-sm-2 control-label">Photo</label>

              <div class="col-sm-8">
                <input type="file" name="photo" id="cat_image">
              </div>
            </div>
          </div>
          <div class="box-footer">
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
          </div>
        </form>
      </div>
    </div>
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Gambar Preview</h3>
        </div>
        <div class="box-body">
          <img src="#" id="category-img-tag" class="img-thumbnail" width="300px" />
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      @foreach($data as $item)
      <div class="col-md-3" style="margin-bottom: 20px;">
        <img src="{{asset('itlabil/images/photo/')}}/{{$item->photo}}" alt="{{ $item->photo }}" class="img-thumbnail">
        <br><br>
        <div class="col-md-12" align="center">
          <form action="{{ route('photo.destroy',$item->id) }}" method="POST">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-danger">Hapus</button>
          </form>
        </div>
      </div>
      @endforeach
    </div>

    <div class="col-md-12" align="center">
      {{ $data->links() }}
    </div>
  </div>

</section>
@endsection