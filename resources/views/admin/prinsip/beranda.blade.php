@extends('layouts.admin')

@section('content')


<div class="container">

    <section class="content-header">
        <h1>
            Prinsip
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Prinsip</h3>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Judul</th>
                                <th>Photo</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($data as $item)
                            <tr>
                                <td>{{ $item->judul }}</td>
                                <td><img src="{{asset('itlabil/images/prinsip/'.$item->photo) }}" class="img-thumbnail" width="300px" alt=""></td>
                                <td align="center">
                                    <a href="{{ route('prinsip.edit', $item->id) }}" class="btn btn-primary">Ubah</a>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection