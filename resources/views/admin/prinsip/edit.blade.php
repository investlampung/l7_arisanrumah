@extends('layouts.admin')

@section('content')

<div class="container">

  <section class="content-header">
    <h1>
      Data Prinsip
    </h1>
  </section><br><br>

  <div class="row">

    <div class="col-md-4">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Edit Data Prinsip</h3>
        </div>
        <form action="{{ route('prinsip.update',$prinsip->id) }}" class="form-horizontal" method="POST" enctype="multipart/form-data">
          @csrf
          @method('PATCH')
          <div class="box-body">
            <div class="form-group">
              <label for="{{$prinsip->judul}}" class="col-sm-4 control-label">{{$prinsip->judul}}</label>

              <div class="col-sm-8">
                <input type="file" name="photo" id="cat_image">
              </div>
            </div>
          </div>
          <div class="box-footer">
            <a href="{{url('admin/prinsip')}}" class="btn btn-default">Kembali</a>
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
          </div>
        </form>
      </div>
    </div>

    <div class="col-md-4">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Gambar Sebelumnya</h3>
        </div>
        <div class="box-body">
          <img src="{{asset('itlabil/images/prinsip/'.$prinsip->photo) }}" class="img-thumbnail" width="300px" alt="">
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Gambar Preview</h3>
        </div>
        <div class="box-body">
          <img src="#" id="category-img-tag" class="img-thumbnail" width="300px" />
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
@endsection