@extends('layouts.admin')

@section('content')


<div class="container">

    <section class="content-header">
        <h1>
            Beranda
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Beranda</h3>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Judul</th>
                                <th>Isi</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($beranda as $item)
                            @if($item->beranda_key==='Paket 2')
                            @elseif($item->beranda_key==='Paket 3')
                            @else
                            <tr>
                                <td>{{ $item->beranda_key }}</td>
                                <td>{!!$item->beranda_value!!}</td>
                                <td align="center">
                                    <a href="{{ route('beranda.edit', $item->id) }}" class="btn btn-primary">Ubah</a>
                                </td>
                            </tr>
                            @endif
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection