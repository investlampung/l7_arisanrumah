@extends('layouts.admin')

@section('content')

<div class="container">

  <section class="content-header">
    <h1>
      Beranda
    </h1>
  </section><br><br>

  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Edit Beranda</h3>
        </div>
        <form action="{{ route('beranda.update',$beranda->id) }}" class="form-horizontal" method="POST">
          @csrf
          @method('PUT')
          <div class="box-body">
            <div class="form-group">
              <label for="{{$beranda->beranda_key}}" class="col-sm-2 control-label">{{$beranda->beranda_key}}</label>

              <div class="col-sm-10">
                <div class="box-body pad">
                  <textarea name="beranda_value" class="textarea" placeholder="Ketik Disini" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px">{{$beranda->beranda_value}}</textarea>
                </div>
              </div>
            </div>
          </div>
          <div class="box-footer">
            <a href="{{url('admin/beranda')}}" class="btn btn-default">Kembali</a>
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
</div>
@endsection