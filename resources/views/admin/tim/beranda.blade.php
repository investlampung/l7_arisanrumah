@extends('layouts.admin')

@section('content')


<div class="container">

  <section class="content-header">
    <h1>
      Tim Marketing
    </h1>
  </section><br><br>

  <div class="row">

    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Tambah Data Tim Marketing</h3>
        </div>
        <form action="{{ route('tim.store') }}" class="form-horizontal" method="POST" enctype="multipart/form-data">
          @csrf
          <div class="box-body">

            <div class="form-group">
              <label for="wilayah" class="col-sm-2 control-label">Wilayah</label>

              <div class="col-sm-10">
                <input type="text" name="wilayah" class="form-control" placeholder="Wilayah">
                <small class="text-danger">{{ $errors->first('wilayah') }}</small>
              </div>
            </div>
            <div class="form-group">
              <label for="nama" class="col-sm-2 control-label">Nama</label>

              <div class="col-sm-10">
                <input type="text" name="nama" class="form-control" placeholder="Nama">
                <small class="text-danger">{{ $errors->first('nama') }}</small>
              </div>
            </div>
            <div class="form-group">
              <label for="jabatan" class="col-sm-2 control-label">Jabatan</label>

              <div class="col-sm-10">
                <input type="text" name="jabatan" class="form-control" placeholder="Jabatan">
                <small class="text-danger">{{ $errors->first('jabatan') }}</small>
              </div>
            </div>
            <div class="form-group">
              <label for="telp" class="col-sm-2 control-label">Telp</label>

              <div class="col-sm-10">
                <input type="text" name="telp" class="form-control" placeholder="HP / Telpon">
                <small class="text-danger">{{ $errors->first('telp') }}</small>
              </div>
            </div>
            <div class="form-group">
              <label for="email" class="col-sm-2 control-label">Email</label>

              <div class="col-sm-10">
                <input type="text" name="email" class="form-control" placeholder="Email">
                <small class="text-danger">{{ $errors->first('email') }}</small>
              </div>
            </div>

            <div class="form-group">
              <label for="photo" class="col-sm-2 control-label">Photo</label>

              <div class="col-sm-8">
                <input type="file" name="photo" id="cat_image">
              </div>
            </div>
          </div>
          <div class="box-footer">
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
          </div>
        </form>
      </div>
    </div>
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Gambar Preview</h3>
        </div>
        <div class="box-body">
          <img src="#" id="category-img-tag" class="img-thumbnail" width="300px" />
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Data Progres</h3>
        </div>

        <div class="box-body" style="overflow-x:auto;">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th>Wilayah</th>
                <th>Nama</th>
                <th>Jabatan</th>
                <th>Telp</th>
                <th>Email</th>
                <th>Photo</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>

              @foreach($data as $item)
              <tr>
                <td>{{ $item->wilayah }}</td>
                <td>{{ $item->nama }}</td>
                <td>{{ $item->jabatan }}</td>
                <td>{{ $item->telp }}</td>
                <td>{{ $item->email }}</td>
                <td><img src="{{asset('itlabil/images/tm/'.$item->photo) }}" class="img-thumbnail" width="150px" alt=""></td>
                <td align="center" width="150px">
                  <form action="{{ route('tim.destroy',$item->id) }}" method="POST">
                    <a href="{{ route('tim.edit', $item->id) }}" class="btn btn-primary">Ubah</a>
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Hapus</button>
                  </form>
                </td>
              </tr>
              @endforeach

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection