@extends('layouts.admin')

@section('content')

<div class="container">

  <section class="content-header">
    <h1>
      Data Tim Marketing
    </h1>
  </section><br><br>

  <div class="row">

    <div class="col-md-4">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Edit Data Tim</h3>
        </div>
        <form action="{{ route('tim.update',$tim->id) }}" class="form-horizontal" method="POST" enctype="multipart/form-data">
          @csrf
          @method('PATCH')
          <div class="box-body">
            <div class="form-group">
              <label for="wilayah" class="col-sm-2 control-label">Wilayah</label>

              <div class="col-sm-10">
                <input type="text" value="{{$tim->wilayah}}" name="wilayah" class="form-control" placeholder="Wilayah">
                <small class="text-danger">{{ $errors->first('wilayah') }}</small>
              </div>
            </div>
            <div class="form-group">
              <label for="nama" class="col-sm-2 control-label">Nama</label>

              <div class="col-sm-10">
                <input type="text" value="{{$tim->nama}}" name="nama" class="form-control" placeholder="Nama">
                <small class="text-danger">{{ $errors->first('nama') }}</small>
              </div>
            </div>
            <div class="form-group">
              <label for="jabatan" class="col-sm-2 control-label">Jabatan</label>

              <div class="col-sm-10">
                <input type="text" value="{{$tim->jabatan}}" name="jabatan" class="form-control" placeholder="Jabatan">
                <small class="text-danger">{{ $errors->first('jabatan') }}</small>
              </div>
            </div>
            <div class="form-group">
              <label for="telp" class="col-sm-2 control-label">Telp</label>

              <div class="col-sm-10">
                <input type="text" value="{{$tim->telp}}" name="telp" class="form-control" placeholder="HP / Telpon">
                <small class="text-danger">{{ $errors->first('telp') }}</small>
              </div>
            </div>
            <div class="form-group">
              <label for="email" class="col-sm-2 control-label">Email</label>

              <div class="col-sm-10">
                <input type="text" value="{{$tim->email}}" name="email" class="form-control" placeholder="Email">
                <small class="text-danger">{{ $errors->first('email') }}</small>
              </div>
            </div>
            <div class="form-group">
              <label for="photo" class="col-sm-4 control-label">Photo</label>

              <div class="col-sm-8">
                <input type="file" name="photo" id="cat_image">
              </div>
            </div>
          </div>
          <div class="box-footer">
            <a href="{{url('admin/tim')}}" class="btn btn-default">Kembali</a>
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
          </div>
        </form>
      </div>
    </div>

    <div class="col-md-4">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Gambar Sebelumnya</h3>
        </div>
        <div class="box-body">
          <img src="{{asset('itlabil/images/tm/'.$tim->photo) }}" class="img-thumbnail" width="300px" alt="">
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Gambar Preview</h3>
        </div>
        <div class="box-body">
          <img src="#" id="category-img-tag" class="img-thumbnail" width="300px" />
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
@endsection