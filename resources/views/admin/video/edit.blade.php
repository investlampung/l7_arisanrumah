@extends('layouts.admin')

@section('content')

<div class="container">

  <section class="content-header">
    <h1>
      Data Video
    </h1>
  </section><br><br>

  <div class="row">
    <div class="col-md-6">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Edit Data Video</h3>
        </div>
        <form action="{{ route('video.update',$video->id) }}" class="form-horizontal" method="POST">
          @csrf
          @method('PUT')
          <div class="box-body">

            <div class="form-group">
              <label for="judul" class="col-sm-2 control-label">Judul</label>

              <div class="col-sm-10">
                <input type="text" value="{{$video->video_key}}" name="video_key" class="form-control" placeholder="Judul">
                <small class="text-danger">{{ $errors->first('video_key') }}</small>
              </div>
            </div>

            <div class="form-group">
              <label for="isi" class="col-sm-2 control-label">Embed Video</label>

              <div class="col-sm-10">
                <input type="text" value="{{$video->video_value}}" name="video_value" class="form-control" placeholder="Embed Video">
                <small class="text-danger">{{ $errors->first('video_value') }}</small>
              </div>
            </div>

          </div>
          <div class="box-footer">
            <a href="{{url('admin/video')}}" class="btn btn-default">Kembali</a>
            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
</div>
@endsection