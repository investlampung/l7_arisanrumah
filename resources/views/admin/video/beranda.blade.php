@extends('layouts.admin')

@section('content')


<div class="container">

    <section class="content-header">
        <h1>
            Video
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Tambah Video</h3>
                </div>
                <form action="{{ route('video.store') }}" class="form-horizontal" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="box-body">

                        <div class="form-group">
                            <label for="judul" class="col-sm-2 control-label">Judul</label>

                            <div class="col-sm-10">
                                <input type="text" name="video_key" class="form-control" placeholder="Judul">
                                <small class="text-danger">{{ $errors->first('video_key') }}</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="isi" class="col-sm-2 control-label">Embed Video</label>

                            <div class="col-sm-10">
                                <input type="text" name="video_value" class="form-control" placeholder="Embed Video">
                                <small class="text-danger">{{ $errors->first('video_value') }}</small>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Video Utama</h3>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Judul</th>
                                <th>Isi</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ $data1->video_key }}</td>
                                <td> <iframe width="260" height="180" src="{{$data1->video_value}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></td>
                                <td align="center">
                                    <a href="{{ route('video.edit', $data1->id) }}" class="btn btn-primary">Ubah</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Video</h3>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Judul</th>
                                <th>Isi</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($data2 as $item)
                            <tr>
                                <td>{{ $item->video_key }}</td>
                                <td> <iframe width="260" height="180" src="{{$item->video_value}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></td>
                                <td align="center">
                                    <form action="{{ route('video.destroy',$item->id) }}" method="POST">
                                        <a href="{{ route('video.edit', $item->id) }}" class="btn btn-primary">Ubah</a>
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection