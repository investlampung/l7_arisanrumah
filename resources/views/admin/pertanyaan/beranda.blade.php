@extends('layouts.admin')

@section('content')

<section class="content-header">
  <h1>
    Pertanyaan
  </h1>
</section>

<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Data Pertanyaan</h3>
        </div>
        <div class="box-body" style="overflow-x:auto;">
          <div class="col-md-12">
            <form action="{{ route('pertanyaan.store') }}" class="form-horizontal" method="POST">
              @csrf

              <div class="form-group">
                <label for="barang" class="col-sm-2 control-label">Pertanyaan</label>

                <div class="col-sm-10">
                  <div class="box-body pad">
                    <textarea name="tanya" class="textarea" placeholder="Ketik Disini" style="width: 100%; height: 100px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px"></textarea>
                  </div>
                  <small class="text-danger">{{ $errors->first('tanya') }}</small>
                </div>
              </div>
              <div class="form-group">
                <label for="satuan" class="col-sm-2 control-label">Jawaban</label>

                <div class="col-sm-10">
                  <div class="box-body pad">
                    <textarea name="jawab" class="textarea" placeholder="Ketik Disini" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px"></textarea>
                  </div>
                  <small class="text-danger">{{ $errors->first('jawab') }}</small>
                </div>
              </div>
              <button type="submit" class="btn btn-primary pull-right">Simpan</button>

            </form>
          </div>
        </div>
        <hr>
        <div class="box-body">
          <div class="box-body" style="overflow-x:auto;">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Tanya</th>
                  <th>Jawab</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach( $data as $item )
                <tr>
                  <td>{{ $no++ }}</td>
                  <td>{!! $item->tanya !!}</td>
                  <td>{!! $item->jawab !!}</td>
                  <td width="130px">
                    <form action="{{ route('pertanyaan.destroy',$item->id) }}" method="POST">
                      <a href="{{ route('pertanyaan.edit', $item->id) }}" class="btn btn-primary">Ubah</a>
                      @csrf
                      @method('DELETE')
                      <button type="submit" class="btn btn-danger">Hapus</button>
                    </form>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection