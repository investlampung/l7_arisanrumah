<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'Web\User\BerandaController@index');
Route::resource('/kontak', 'Web\User\KontakController');
Route::resource('/laporan', 'Web\User\LaporanController');
Route::post('/pesan/store', 'Web\User\BerandaController@store');
Route::get('/gallery', 'Web\User\BerandaController@gallery');
Route::get('/video', 'Web\User\BerandaController@video');
Route::get('download/katalog', 'Web\User\BerandaController@download');
Route::get('/hubungi', 'Web\User\BerandaController@hubungi');

Auth::routes(['register' => false]);

Route::resource('/admin/beranda', 'Web\Admin\BerandaController');
Route::resource('/admin/profil', 'Web\Admin\ProfilController');
Route::resource('/admin/prinsip', 'Web\Admin\PrinsipController');
Route::resource('/admin/gambar', 'Web\Admin\GambarController');
Route::resource('/admin/video', 'Web\Admin\VideoController');
Route::resource('/admin/pesan', 'Web\Admin\PesanController');
Route::resource('/admin/photo', 'Web\Admin\GalleryController');
Route::resource('/admin/history', 'Web\Admin\HistoryController');
Route::resource('/admin/progres', 'Web\Admin\ProgresController');
Route::resource('/admin/testimoni', 'Web\Admin\TestimoniController');
Route::resource('/admin/pertanyaan', 'Web\Admin\PertanyaanController');
Route::resource('/admin/tim', 'Web\Admin\TimController');
Route::resource('/admin/file', 'Web\Admin\FileController');
Route::get('/admin/cetak/pesan/print/{id}', 'Web\Admin\PesanController@pesan');
Route::get('/admin/cetak/pesan-semua/print', 'Web\Admin\PesanController@pesansemua');
