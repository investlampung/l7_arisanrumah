-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 05, 2020 at 06:02 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_rumaharisan`
--

-- --------------------------------------------------------

--
-- Table structure for table `albums`
--

CREATE TABLE `albums` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `album` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `berandas`
--

CREATE TABLE `berandas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `beranda_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `beranda_value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `berandas`
--

INSERT INTO `berandas` (`id`, `beranda_key`, `beranda_value`, `created_at`, `updated_at`) VALUES
(1, 'Harga 1', 'Rp. 410.000', '2020-08-01 21:46:40', '2020-08-01 21:52:34'),
(2, 'Harga 2', 'Rp. 600.000', '2020-08-01 21:46:40', '2020-08-01 21:46:40'),
(3, 'Harga 3', 'Rp. 1.200.000', '2020-08-01 21:46:40', '2020-08-01 21:46:40'),
(4, 'Katalog', '<p>Informasi Rinci Rumah Arisan<br>Dan Lain Lain</p>', '2020-08-01 21:46:40', '2020-08-01 21:46:40'),
(5, 'Rekening', '<p>BRI : 7725 01 0055 3453 0<br>AN : Mitra Griya Indonesia</p>', '2020-08-01 21:46:40', '2020-08-01 21:46:40'),
(6, 'NPWP', '<p>NPWP : 93.581.577.9-325.000<br>Mitra Griya Indonesia</p>', '2020-08-01 21:46:40', '2020-08-01 21:46:40'),
(7, 'Usaha', '<p>Nomor Induk Usaha : 0220201142075<br>Pengesahan Kemkumham : AHU-00045.AH.01.26</p>', '2020-08-01 21:46:40', '2020-08-01 21:46:40'),
(8, 'Cara Bergabung', '<p>\r\n              - Mendapatkan persetujuan keluarga<br>\r\n              - Anggota aktif adalah istri/ perempuan<br>\r\n              - Mempunyai lahan/tanah sendiri yang sudah diratakan dan siap dibangun<br>\r\n              - Posisi tanah harus rata/ urugan menjadi tanggung jawab pemilik<br>\r\n              - Menyerahkan sertifikat/surat tanah sebagai jaminan sementara setelah rumah\r\n              sudah terbangun dan akan dikembalikan jika pembayaran sudah lunas<br>\r\n              - Mengisi formulir calon peserta Arisan Rumah<br>\r\n              - Rutin menyetor angsuran setiap tanggal 5 s/d 15. bisa di setor langsung ke\r\n              kantor kami atau via transfer ke rekening BRI : 7725 01 0055 34 53 0 atan nama :\r\n              Mitra Griya Indonesia<br>\r\n              - Tidak merubah material yang telah disepakati<br>\r\n              - Pembangunan rumah dilaksanakan bergilir sesuai sistem kocokan/undian<br>\r\n              - Jika mendapatkan undian maka Arisan tidak dapat diambil dalam bentuk uang<br>\r\n              - Bersedia dibongkar jika sudah disimpulkan oleh peserta lain tidak ada\r\n              itikatbaik untuk melunasi arisan<br>\r\n              - Jika mengundurkan diri peserta wajib mencari pengganti apabila tidak bisa maka\r\n              uang angusuran akan dikembalikan jika putaran arisan sudah selesai<br>\r\n              - Setuju setiap tahun ada penyesuaian harga material (inflasi)<br>\r\n            </p>', '2020-08-01 17:00:00', '2020-08-01 17:00:00'),
(9, 'Paket 1', '<p>\r\n              Dengan dana 400.000 / bulan yang anda dapatkan adalah rumah standar mitra griya\r\n              ukuran 6 x\r\n              7 dengan fasilitas 2 kamar tidur, 1 ruang keluarga, 1 ruang tamu, biaya tukang\r\n              sudah di\r\n              cover, Spesifikasi detil bisa dilihat disini (lihat spesifikasi) dengan\r\n              Mekanisme\r\n              pembiayaan sebagai berikut :<br><br>\r\n              - pembayaran dilakukan selama 75 kali (bulan)<br>\r\n              - anggota 25 orang / kelompok<br>\r\n              - pembangunan rumah akan dilaksanakan 3 bulan sekali (untuk memenuhi dana 30\r\n              jt/kelompok)\r\n            </p>', '2020-08-01 17:00:00', '2020-08-01 17:00:00'),
(10, 'Paket 2', '<p>\r\n              Dengan dana 600.000 / bulan yang anda dapatkan adalah rumah standar mitra griya\r\n              ukuran 6 x\r\n              7 dengan fasilitas 2 kamar tidur, 1 ruang keluarga, 1 ruang tamu, biaya tukang\r\n              sudah di\r\n              cover, Spesifikasi detil bisa dilihat disini (lihat spesifikasi) dengan\r\n              mekanisme pembiayaan sebagai berikut :<br><br>\r\n              - pembayaran dilakukan selama 50 kali (bulan)<br>\r\n              - anggota 25 orang / kelompok<br>\r\n              - pembangunan rumah akan dilaksanakan 2 bulan sekali (untuk memenuhi dana 30\r\n              jt/kelompok)\r\n            </p>', '2020-08-01 17:00:00', '2020-08-01 17:00:00'),
(11, 'Paket 3', '<p>\r\n              Dengan dana 1.200.000 / bulan yang anda dapatkan adalah rumah standar mitra\r\n              griya ukuran 6\r\n              x 7 dengan fasilitas 2 kamar tidur, 1 ruang keluarga, 1 ruang tamu, biaya tukang\r\n              sudah di\r\n              cover, Spesifikasi detil bisa dilihat disini (lihat spesifikasi) adapun\r\n              mekanisme\r\n              pembiayaan sebagai berikut :<br><br>\r\n              - pembayaran dilakukan selama 25 kali (bulan)<br>\r\n              - anggota 25 orang / kelompok<br>\r\n              - pembangunan rumah akan dilaksanakan 1 bulan sekali (untuk memenuhi dana 30\r\n              jt/kelompok)\r\n            </p>', '2020-08-01 17:00:00', '2020-08-01 17:00:00'),
(12, 'Siapa Kami', '<p>\r\n              arisanrumah.id adalah komunitas binaan Koperasi Mitra Griya. koperasi ini sudah\r\n              memiliki ijin resmi dari dinas koperasi kabupaten pringsewu. arisan binaan\r\n              koperasi mitra griya indonesia sudah mencapai 9 kelompok arisan. seluruh\r\n              tanggung jawab dan resiko yang ada didalam komunitas arisanrumah.id sudah di\r\n              cover oleh koperasi mitra griya indonesia.\r\n            </p>', '2020-08-01 17:00:00', '2020-08-01 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gambars`
--

CREATE TABLE `gambars` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `gambar_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gambar_value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gambars`
--

INSERT INTO `gambars` (`id`, `gambar_key`, `gambar_value`, `created_at`, `updated_at`) VALUES
(1, 'Slide 1', 'BEl2EurMab.jpeg', '2020-08-01 17:00:00', '2020-08-03 03:19:19'),
(2, 'Slide 2', 'slider-2.jpg', '2020-08-01 17:00:00', '2020-08-01 17:00:00'),
(3, 'Slide 3', 'slider-3.jpg', '2020-08-01 17:00:00', '2020-08-01 17:00:00'),
(4, 'Slide 4', 'slider-4.jpg', '2020-08-01 17:00:00', '2020-08-01 17:00:00'),
(5, 'Gambar Hubungi', 'hubungi.jpg', '2020-08-01 17:00:00', '2020-08-01 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `histories`
--

CREATE TABLE `histories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `info` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc_info` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `histories`
--

INSERT INTO `histories` (`id`, `user`, `info`, `desc_info`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 'Ubah', 'Beranda Harga 1 menjadi Rp. 410.000', '2020-08-01 21:52:34', '2020-08-01 21:52:34'),
(2, 'Restu Aji Kesuma', 'Tambah', 'User Restu Aji Kesuma mengirim pesan fsdfgfdgdfg dfg dfsg dfs', '2020-08-01 23:18:19', '2020-08-01 23:18:19'),
(3, 'Anggi Bastiansyah', 'Tambah', 'User Anggi Bastiansyah mengirim pesan yffjhgf gf jgf', '2020-08-01 23:24:53', '2020-08-01 23:24:53'),
(4, 'Administrator', 'Ubah', 'Gambar Slide 1 menjadi Inu27ikAuF.jpeg', '2020-08-03 03:18:19', '2020-08-03 03:18:19'),
(5, 'Administrator', 'Ubah', 'Gambar Slide 1 menjadi BEl2EurMab.jpeg', '2020-08-03 03:19:19', '2020-08-03 03:19:19'),
(6, 'Administrator', 'Ubah', 'Profil Telp2 menjadi 089631232434 (Admin1)', '2020-08-03 03:21:47', '2020-08-03 03:21:47'),
(7, 'Administrator', 'Hapus', 'Data Pesan Restu Aji Kesuma', '2020-08-03 03:33:54', '2020-08-03 03:33:54'),
(8, 'Administrator', 'Tambah', 'Data Progres Rumah Erwin', '2020-08-03 04:07:34', '2020-08-03 04:07:34'),
(9, 'Administrator', 'Tambah', 'Data Pertanyaan <p>Mau Tanya Apa?</p>', '2020-08-03 04:45:53', '2020-08-03 04:45:53'),
(10, 'Administrator', 'Ubah', 'Pertanyaan <p>Mau Tanya Apa?</p> berhasil diubah.', '2020-08-03 05:14:00', '2020-08-03 05:14:00'),
(11, 'Administrator', 'Ubah', 'Gambar Transparan dan Amanah berhasil diubah ', '2020-08-03 05:47:39', '2020-08-03 05:47:39'),
(12, 'Administrator', 'Tambah', 'Data Tim Marketing Anggi Bastiansyah', '2020-08-03 06:13:29', '2020-08-03 06:13:29'),
(13, 'Administrator', 'Ubah', 'Data Tim Marketing Anggi Bastiansyah berhasil diubah', '2020-08-03 06:36:46', '2020-08-03 06:36:46'),
(14, 'Administrator', 'Hapus', 'Data Tim Marketing Anggi Bastiansyah', '2020-08-03 06:44:25', '2020-08-03 06:44:25'),
(15, 'Administrator', 'Tambah', 'Photo Gallery ', '2020-08-03 07:06:14', '2020-08-03 07:06:14'),
(16, 'Administrator', 'Hapus', 'Photo Gallery', '2020-08-03 07:19:06', '2020-08-03 07:19:06'),
(17, 'Administrator', 'Tambah', 'Data File', '2020-08-03 07:56:33', '2020-08-03 07:56:33'),
(18, 'Administrator', 'Ubah', 'Data File', '2020-08-03 07:57:43', '2020-08-03 07:57:43'),
(19, 'Administrator', 'Hapus', 'Data Progress Rumah Erwin', '2020-08-04 20:22:01', '2020-08-04 20:22:01'),
(20, 'Restu Aji Kesuma', 'Tambah', 'User Restu Aji Kesuma mengirim pesan Coba jhkjsajdh jhd asjdh asjdh asdjh asljdh asjdh asjdh lasjdh lsajdh lasjdh ljasdh sajdh lasjdh lasjdh ljas hdljashdlas la hlds', '2020-08-04 20:52:11', '2020-08-04 20:52:11');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(12, '2014_10_12_000000_create_users_table', 1),
(13, '2014_10_12_100000_create_password_resets_table', 1),
(14, '2019_08_19_000000_create_failed_jobs_table', 1),
(15, '2020_08_02_041253_create_berandas_table', 1),
(16, '2020_08_02_041337_create_gambars_table', 1),
(17, '2020_08_02_041351_create_histories_table', 1),
(18, '2020_08_02_041408_create_pesans_table', 1),
(19, '2020_08_02_041418_create_profils_table', 1),
(20, '2020_08_02_041430_create_videos_table', 1),
(21, '2020_08_02_041440_create_albums_table', 1),
(22, '2020_08_02_041501_create_photos_table', 1),
(23, '2020_08_02_044203_create_phots_table', 1),
(24, '2020_08_02_044212_create_prinsips_table', 1),
(25, '2020_08_02_055716_create_progres_table', 2),
(26, '2020_08_02_060723_create_pertanyaans_table', 3),
(27, '2020_08_02_085802_create_tims_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pertanyaans`
--

CREATE TABLE `pertanyaans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tanya` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `jawab` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pertanyaans`
--

INSERT INTO `pertanyaans` (`id`, `tanya`, `jawab`, `created_at`, `updated_at`) VALUES
(1, 'Apa syarat yang harus saya siapkan jika ingin ikut arisan rumah ?', '1. Sudah punya lahan/tanah<br>\r\n            2. Mendapat persetujuan keluarga<br>\r\n            3. Mengisi formulir<br>\r\n            4. Mengumpulkan copi ktp & kk<br>\r\n            5. Membayar biaya pendaftaran rp 500 rb<br><br>', '2020-08-01 17:00:00', '2020-08-01 17:00:00'),
(2, 'Apa yang saya dapatkan jika ikut arisan rumah ?', '1 unit rumah ukuran 6x7m terdiri dari r.tamu, 2 kamar tidur, r.keluarga dan\r\n            teras.<br>\r\n            Rumah tersebut belum finishing (bata merah), lantai floor kasar, namun siap huni<br>\r\n            klik disini untuk melihat spesifikasi dan untuk melihat bentuk bangunan bisa di klik\r\n            disini<br>', '2020-08-01 17:00:00', '2020-08-01 17:00:00'),
(3, '<p>Mau Tanya Apa?</p>', '<p>Pasti Dijawab. asli</p>', '2020-08-03 04:45:53', '2020-08-03 05:14:00');

-- --------------------------------------------------------

--
-- Table structure for table `pesans`
--

CREATE TABLE `pesans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pesan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pesans`
--

INSERT INTO `pesans` (`id`, `nama`, `email`, `telp`, `pesan`, `created_at`, `updated_at`) VALUES
(2, 'Anggi Bastiansyah', 'njajal.studio8@gmail.com', '089631073926', 'yffjhgf gf jgf', '2020-08-01 23:24:53', '2020-08-01 23:24:53'),
(3, 'Restu Aji Kesuma', 'njajal.studio8@gmail.com', '089631073911', 'Coba jhkjsajdh jhd asjdh asjdh asdjh asljdh asjdh asjdh lasjdh lsajdh lasjdh ljasdh sajdh lasjdh lasjdh ljas hdljashdlas la hlds', '2020-08-04 20:52:11', '2020-08-04 20:52:11');

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE `photos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`id`, `photo`, `created_at`, `updated_at`) VALUES
(1, 'image1.jpg', '2020-08-01 17:00:00', '2020-08-01 17:00:00'),
(2, 'image2.jpg', '2020-08-01 17:00:00', '2020-06-04 17:00:00'),
(3, 'image3.jpg', '2020-08-01 17:00:00', '2020-08-01 17:00:00'),
(4, 'image4.jpg', '2020-08-01 17:00:00', '2020-08-01 17:00:00'),
(5, 'image1.jpg', '2020-08-01 17:00:00', '2020-08-01 17:00:00'),
(6, 'image2.jpg', '2020-08-01 17:00:00', '2020-06-04 17:00:00'),
(7, 'image3.jpg', '2020-08-01 17:00:00', '2020-08-01 17:00:00'),
(8, 'image4.jpg', '2020-08-01 17:00:00', '2020-08-01 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `phots`
--

CREATE TABLE `phots` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `prinsips`
--

CREATE TABLE `prinsips` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `judul` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ket` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `prinsips`
--

INSERT INTO `prinsips` (`id`, `judul`, `photo`, `ket`, `created_at`, `updated_at`) VALUES
(1, 'Transparan dan Amanah', 'jFBf83CHts.jpeg', '<p>Setiap bulan laporan arisan per kelompok akan kami sampaikan dan kirimkan kepada\r\n              masing-masing anggota tanpa ada yang disembunyikan. Kami akan menjaga privasi\r\n              data pribadi anda yang terdaftar di arisanrumah.id. untuk mengamankan anggota\r\n              arisan yang sudah dibangunkan rumahnya maka sertifikat/surat tanah akan ditahan\r\n              oleh koperasi mitra griya indonesia sampai arisan selesai semuanya.</p>', '2020-08-01 17:00:00', '2020-08-03 05:47:39'),
(2, 'Pembiayaan Mudah', 'image2.jpg', '<p>Pembangunan program rumah ini menggunakan pola arisan yang dibentuk 1 kelompok beranggotakan 25 orang sesuai dengan paket yang dipilih.</p>', '2020-08-01 17:00:00', '2020-08-01 17:00:00'),
(3, 'Gotong Royong dan Kekeluargaan', 'image3.jpg', '<p>arisanrumah.id menggunakan prinsip gotong royong dan kekeluargaan, saling membantu, dan saling mufakat.</p>', '2020-08-01 17:00:00', '2020-08-01 17:00:00'),
(4, '0 Rupiah Denda', 'image4.jpg', '<p>Jika ada keterlambatan pembayaran maka komunitas arisanrumah.id tidak\r\n              memberlakukan biaya denda sama sekali. tentunya jika anggota komunitas\r\n              arisanrumah.id masih ada itikad baik untuk melunasi tunggakan arisan.</p>', '2020-08-01 17:00:00', '2020-08-01 17:00:00'),
(5, 'Berkualitas', 'image5.jpg', '<p>Bahan bangunan yang kami sediakan untuk program ini adalah hasil produksi dari\r\n              UMKM mitra griya. dengan kualitas yang sudah teruji dan terjamin.</p>', '2020-08-01 17:00:00', '2020-08-01 17:00:00'),
(6, 'Harga Terjangkau', 'image6.jpg', '<p>Harga yang kami berikan cenderung terjangkau oleh masyarakat kalangan menengah\r\n              kebawah.</p>', '2020-08-01 17:00:00', '2020-08-01 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `profils`
--

CREATE TABLE `profils` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `profil_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profil_value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profils`
--

INSERT INTO `profils` (`id`, `profil_key`, `profil_value`, `created_at`, `updated_at`) VALUES
(1, 'Nama', 'Rumah Arisan', '2020-08-01 21:46:40', '2020-08-01 21:46:40'),
(2, 'Alamat 1', 'Pringsewu - Lampung', '2020-08-01 21:46:40', '2020-08-01 21:46:40'),
(3, 'Alamat 2', 'Jl. Satria Gading Rejo Kec. Gading Rejo Kode Pos 35372', '2020-08-01 21:46:40', '2020-08-01 21:46:40'),
(4, 'Jam Kerja', 'Senin - Sabtu 09:00 - 17:00 Minggu Tutup', '2020-08-01 21:46:40', '2020-08-01 21:46:40'),
(5, 'Email', 'mitragriya01@gmail.com', '2020-08-01 21:46:40', '2020-08-01 21:46:40'),
(6, 'Telp', '082269400528 (Yudi)', '2020-08-01 21:46:40', '2020-08-01 21:46:40'),
(7, 'Youtube', '#', '2020-08-01 21:46:40', '2020-08-01 21:46:40'),
(8, 'Instagram', '#', '2020-08-01 21:46:40', '2020-08-01 21:46:40'),
(9, 'Facebook', '#', '2020-08-01 21:46:40', '2020-08-01 21:46:40'),
(10, 'Whatsapp', '#', '2020-08-01 21:46:40', '2020-08-01 21:46:40'),
(11, 'Telp2', '089631232434 (Admin1)', '2020-08-01 17:00:00', '2020-08-03 03:21:47');

-- --------------------------------------------------------

--
-- Table structure for table `progres`
--

CREATE TABLE `progres` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `judul` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `progres`
--

INSERT INTO `progres` (`id`, `judul`, `photo`, `created_at`, `updated_at`) VALUES
(1, 'Rumah Bapak Diki', 'image1.jpg', '2020-08-01 17:00:00', '2020-08-01 17:00:00'),
(2, 'Rumah Bapak Abu Bakar', 'image2.jpg', '2020-08-01 17:00:00', '2020-08-01 17:00:00'),
(3, 'Rumah Bapak Oman Sulaiman', 'image3.jpg', '2020-08-01 17:00:00', '2020-08-01 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tims`
--

CREATE TABLE `tims` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `wilayah` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jabatan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tims`
--

INSERT INTO `tims` (`id`, `wilayah`, `nama`, `jabatan`, `telp`, `email`, `photo`, `created_at`, `updated_at`) VALUES
(1, 'TM Pringsewu', 'Abdul Hidayat', 'Production manager', '089631072233', 'abdul@gmail.com', 'avatar.png', '2020-08-01 17:00:00', '2020-08-01 17:00:00'),
(2, 'TM Pringsewu', 'Abdul Hidayat', 'Production manager', '089631072233', 'abdul@gmail.com', 'avatar.png', '2020-08-01 17:00:00', '2020-06-04 17:00:00'),
(3, 'TM Pringsewu', 'Abdul Hidayat', 'Production manager', '089631072233', 'abdul@gmail.com', 'avatar.png', '2020-08-01 17:00:00', '2020-08-01 17:00:00'),
(4, 'TM Pringsewu', 'Abdul Hidayat', 'Production manager', '089631072233', 'abdul@gmail.com', 'avatar.png', '2020-08-01 17:00:00', '2020-08-01 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 'admin@sourcemedia.id', NULL, '$2y$10$y//K953ZDjJZvUbUPjTZze1UEB7rZA.e.wjzxW2kR3LIU/nFnPPh.', NULL, '2020-08-01 21:46:40', '2020-08-01 21:46:40'),
(2, 'Admin AR', 'admin@arisanrumah.id', NULL, '$2y$10$yFKrd10ysEJoXo0wbwvQM.5cswsoKFggYbXTj9bPNXYpjJuN3BELq', NULL, '2020-08-01 21:46:40', '2020-08-01 21:46:40');

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `video_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `video_value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `video_key`, `video_value`, `created_at`, `updated_at`) VALUES
(1, 'Cara Bergabung', 'https://www.youtube.com/embed/VEzws9X3bw4', '2020-08-01 17:00:00', '2020-08-01 17:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `albums`
--
ALTER TABLE `albums`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `berandas`
--
ALTER TABLE `berandas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gambars`
--
ALTER TABLE `gambars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `histories`
--
ALTER TABLE `histories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pertanyaans`
--
ALTER TABLE `pertanyaans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pesans`
--
ALTER TABLE `pesans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `phots`
--
ALTER TABLE `phots`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prinsips`
--
ALTER TABLE `prinsips`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profils`
--
ALTER TABLE `profils`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `progres`
--
ALTER TABLE `progres`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tims`
--
ALTER TABLE `tims`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `albums`
--
ALTER TABLE `albums`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `berandas`
--
ALTER TABLE `berandas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gambars`
--
ALTER TABLE `gambars`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `histories`
--
ALTER TABLE `histories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `pertanyaans`
--
ALTER TABLE `pertanyaans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pesans`
--
ALTER TABLE `pesans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `photos`
--
ALTER TABLE `photos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `phots`
--
ALTER TABLE `phots`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `prinsips`
--
ALTER TABLE `prinsips`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `profils`
--
ALTER TABLE `profils`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `progres`
--
ALTER TABLE `progres`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tims`
--
ALTER TABLE `tims`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
