<?php

use Illuminate\Database\Seeder;

class BerandaSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Beranda::create([
            'beranda_key' => 'Harga 1',
            'beranda_value' => 'Rp. 400.000'
        ]);
        App\Beranda::create([
            'beranda_key' => 'Harga 2',
            'beranda_value' => 'Rp. 600.000'
        ]);
        App\Beranda::create([
            'beranda_key' => 'Harga 3',
            'beranda_value' => 'Rp. 1.200.000'
        ]);
        App\Beranda::create([
            'beranda_key' => 'Katalog',
            'beranda_value' => '<p>Informasi Rinci Rumah Arisan<br>Dan Lain Lain</p>'
        ]);
        App\Beranda::create([
            'beranda_key' => 'Rekening',
            'beranda_value' => '<p>BRI : 7725 01 0055 3453 0<br>AN : Mitra Griya Indonesia</p>'
        ]);
        App\Beranda::create([
            'beranda_key' => 'NPWP',
            'beranda_value' => '<p>NPWP : 93.581.577.9-325.000<br>Mitra Griya Indonesia</p>'
        ]);
        App\Beranda::create([
            'beranda_key' => 'Usaha',
            'beranda_value' => '<p>Nomor Induk Usaha : 0220201142075<br>Pengesahan Kemkumham : AHU-00045.AH.01.26</p>'
        ]);
    }
}
